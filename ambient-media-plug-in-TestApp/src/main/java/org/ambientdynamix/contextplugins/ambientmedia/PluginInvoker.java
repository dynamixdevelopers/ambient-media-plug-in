package org.ambientdynamix.contextplugins.ambientmedia;

import android.app.Activity;
import android.content.ServiceConnection;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.os.Environment;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import org.ambientdynamix.api.application.*;
import org.ambientdynamix.contextplugins.ambientcontrol.IControlRequest;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * Created by workshop on 11/6/13.
 */
public class PluginInvoker {


    private List<PluginInvocation> pluginInvocations;

    /**
     * if this is set to true the BindDynamixActivity will not require any user interaction to run
     */
    public static final boolean AUTOMATIC_EXECUTION = false;

    private String TAG = this.getClass().getSimpleName();




    /**
     * config end
     */
    private DynamixFacade dynamix;
    private BindDynamixActivity activity;

    /**
     *
     * @param dynamix
     * @param activity
     */
    public PluginInvoker(DynamixFacade dynamix, BindDynamixActivity activity) {
        this.dynamix = dynamix;

        this.activity = activity;
        pluginInvocations = new Vector<PluginInvocation>();
        addPluginInvocation("org.ambientdynamix.contextplugins.ambientmedia", "org.ambientdynamix.contextplugins.ambientmedia.discovery",null);
        addPluginInvocation("org.ambientdynamix.contextplugins.ambientmedia", "org.ambientdynamix.contextplugins.ambientmedia.playbackinfo",null);
        addPluginInvocation("org.ambientdynamix.contextplugins.ambientmedia", "org.ambientdynamix.contextplugins.ambientmedia.interaction",null);
        addPluginInvocation("org.ambientdynamix.contextplugins.spheronative", IControlRequest.CONTEXT_TYPE,null);

    }







    /**
     * This gets called when the DynamixBindActivity receives a context event of a type specified in the cotextRequestType array
     *
     * @param event
     */
    public void invokeOnResponse(ContextResult event) {

        // Check for native IContextInfo
        if (event.hasIContextInfo()) {
            Log.i(TAG, "A1 - Event contains native IContextInfo: " + event.getIContextInfo());
            IContextInfo nativeInfo = event.getIContextInfo();
                /*
				 * Note: At this point you can cast the IContextInfo into it's native type and then call its methods. In
				 * order for this to work, you'll need to have the proper Java classes for the IContextInfo data types
				 * on your app's classpath. If you don't, event.hasIContextInfo() will return false and
				 * event.getIContextInfo() would return null, meaning that you'll need to rely on the string
				 * representation of the context info. To use native context data-types, simply download the data-types
				 * JAR for the Context Plug-in you're interested in, include the JAR(s) on your build path, and you'll
				 * have access to native context type objects instead of strings.
				 */
            Log.i(TAG, "A1 - IContextInfo implementation class: " + nativeInfo.getImplementingClassname());
            // Example of using IPedometerStepInfo


            // Check for other interesting types, if needed...
            if(event.getIContextInfo() instanceof AmbientMediaPlaybackInfo){
                final AmbientMediaPlaybackInfo info = (AmbientMediaPlaybackInfo) event.getIContextInfo();
                Log.i(TAG,"received playback info: position(" +info.getPosition() + ") at rate(" + info.getRate() + ")");
            }


        } else
            Log.i(TAG,
                    "Event does NOT contain native IContextInfo... we need to rely on the string representation!");
    }

    /**
     *
     * @param pluginId    The plugin to call
     * @param contextType  The contextType  to request
     * @param configuration  optional configuration bundle, can be null
     */
    private void addPluginInvocation(String pluginId, String contextType, Bundle configuration) {
        pluginInvocations.add(new PluginInvocation(pluginId,contextType,configuration));
    }

    public List<PluginInvocation> getPluginInvocations() {
        return pluginInvocations;
    }

    public class PluginInvocation {
        private String pluginId;
        private String contextRequestType;
        private Bundle configuration;
        private boolean successfullyCalled;

        public PluginInvocation(String pluginId, String contextRequestType, Bundle configuration) {
            this.pluginId = pluginId;
            this.contextRequestType = contextRequestType;
            this.configuration = configuration;
        }

        public String getPluginId() {
            return pluginId;
        }

        public void setPluginId(String pluginId) {
            this.pluginId = pluginId;
        }

        public String getContextRequestType() {
            return contextRequestType;
        }

        public void setContextRequestType(String contextRequestType) {
            this.contextRequestType = contextRequestType;
        }

        public Bundle getConfiguration() {
            return configuration;
        }

        public void setConfiguration(Bundle configuration) {
            this.configuration = configuration;
        }

        public boolean isSuccessfullyCalled() {
            return successfullyCalled;
        }

        public void setSuccessfullyCalled(boolean successfullyCalled) {
            this.successfullyCalled = successfullyCalled;
        }
    }

}
