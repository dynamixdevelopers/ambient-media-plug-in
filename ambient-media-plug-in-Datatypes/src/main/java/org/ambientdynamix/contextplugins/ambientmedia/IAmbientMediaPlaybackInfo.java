package org.ambientdynamix.contextplugins.ambientmedia;

import org.ambientdynamix.api.application.IContextInfo;

/**
 * Created by workshop on 12/17/13.
 */
public interface IAmbientMediaPlaybackInfo extends IContextInfo {
    double getPosition();

    void setPosition(double position);

    double getRate();

    void setRate(double rate);
}
