package org.ambientdynamix.contextplugins.ambientmedia;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashSet;
import java.util.Set;

/**
 * This context type provides Information about the current playback status.
 * @author Max Pagel
 */
public class AmbientMediaPlaybackInfo implements IAmbientMediaPlaybackInfo {

    public static final String CONTEXT_TYPE = "org.ambientdynamix.contextplugins.ambientmedia.playbackinfo";

    /**
     * Required CREATOR field that generates instances of this Parcelable class from a Parcel.
     *
     * @see http://developer.android.com/reference/android/os/Parcelable.Creator.html
     */
    public static final Parcelable.Creator<AmbientMediaPlaybackInfo> CREATOR = new Parcelable.Creator<AmbientMediaPlaybackInfo>() {
        public AmbientMediaPlaybackInfo createFromParcel(Parcel in) {
            return new AmbientMediaPlaybackInfo(in);
        }

        public AmbientMediaPlaybackInfo[] newArray(int size) {
            return new AmbientMediaPlaybackInfo[size];
        }
    };

    private double position;
    private double rate;

    public AmbientMediaPlaybackInfo(Parcel in) {
        this.position = in.readDouble();
        this.rate = in.readDouble();
    }

    /**
     * returns the current position in the movie in seconds
     * @return the current position in seconds
     */
    @Override
    public double getPosition() {
        return position;
    }

    /**
     * set the current position in the movie in seconds
     * @param position
     */
    @Override
    public void setPosition(double position) {
        this.position = position;
    }

    /**
     * returns the current playback rate. Normal playback rate is 1
     * @return the playback rate
     */
    @Override
    public double getRate() {
        return rate;
    }

    /**
     * set the current playback rate
     * @param rate
     */
    @Override
    public void setRate(double rate) {
        this.rate = rate;
    }

    @Override
    public String getContextType() {
        return CONTEXT_TYPE;
    }

    @Override
    public String getImplementingClassname() {
        return this.getClass().getName();
    }

    @Override
    public Set<String> getStringRepresentationFormats() {
        Set<String> formats = new HashSet<String>();
        formats.add("dynamix/web");
        return formats;
    }

    @Override
    public String getStringRepresentation(String s) {
        return this.getClass().getSimpleName();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(position);
        dest.writeDouble(rate);
    }

    public AmbientMediaPlaybackInfo(double position, double rate) {
        this.position = position;
        this.rate = rate;
    }
}
