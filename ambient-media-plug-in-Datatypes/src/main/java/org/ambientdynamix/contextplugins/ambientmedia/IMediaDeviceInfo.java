/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.ambientmedia;

import org.ambientdynamix.api.application.IContextInfo;

/**
 * Simple representation of an ambient media device.
 * 
 * @author Darren Carlson
 * 
 */
public interface IMediaDeviceInfo extends IContextInfo {
	/**
	 * Returns the unique id of the device.
	 */
	public String getId();

	/**
	 * Returns the display name of the device.
	 */
	public String getDisplayName();

	/**
	 * Returns the type of the device: MediaRenderer, MediaServer, etc.
	 */
	public String getType();

	/**
	 * Indicates if this device was discovered (available == true) or lost (available == false).
	 */
	public boolean isAvailable();
}