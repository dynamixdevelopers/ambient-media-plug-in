/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.ambientmedia;

import java.util.HashSet;
import java.util.Set;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Simple representation of a media device.
 * 
 * @author Darren Carlson
 * 
 */
public class MediaDeviceInfo implements IMediaDeviceInfo {
	/**
	 * Required CREATOR field that generates instances of this Parcelable class from a Parcel.
	 * 
	 * @see http://developer.android.com/reference/android/os/Parcelable.Creator.html
	 */
	public static final Parcelable.Creator<MediaDeviceInfo> CREATOR = new Parcelable.Creator<MediaDeviceInfo>() {
		public MediaDeviceInfo createFromParcel(Parcel in) {
			return new MediaDeviceInfo(in);
		}

		public MediaDeviceInfo[] newArray(int size) {
			return new MediaDeviceInfo[size];
		}
	};
	private String id;
	private String displayName;
	private String type;
	private boolean available;

	public MediaDeviceInfo() {
	}

	/**
	 * Creates a MediaDeviceInfo.
	 */
	public MediaDeviceInfo(String id, String displayName, String type, boolean available) {
		this.id = id;
		this.displayName = displayName;
		this.type = type;
		this.available = available;
	}

	/**
	 * {@inheritDoc}
	 */
	public String getId() {
		return id;
	}

	void setId(String id) {
		this.id = id;
	}

	/**
	 * {@inheritDoc}
	 */
	public String getDisplayName() {
		return displayName;
	}

	void setDisplayName(String name) {
		this.displayName = name;
	}

	/**
	 * {@inheritDoc}
	 */
	public String getType() {
		return type;
	}

	void setType(String type) {
		this.type = type;
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean isAvailable() {
		return available;
	}

	void setAvailable(boolean available) {
		this.available = available;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<String> getStringRepresentationFormats() {
		Set<String> formats = new HashSet<String>();
		formats.add("text/plain");
		return formats;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getStringRepresentation(String format) {
		if (format.equalsIgnoreCase("text/plain"))
			return "MediaDevice: id=" + id + ", displayName=" + displayName + ", type=" + type + ", available=" + available;
		else
			return "";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getImplementingClassname() {
		return this.getClass().getName();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getContextType() {
		return "org.ambientdynamix.contextplugins.ambientmedia.discovery";
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName();
	};

	public IBinder asBinder() {
		return null;
	}

	public int describeContents() {
		return 0;
	}

	public void writeToParcel(Parcel out, int arg1) {
		out.writeString(id);
		out.writeString(displayName);
		out.writeString(type);
		out.writeByte((byte) (available ? 1 : 0));
	}

	private MediaDeviceInfo(final Parcel in) {
		id = in.readString();
		displayName = in.readString();
		type = in.readString();
		available = in.readByte() == 1;
	}
}
