/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.ambientmedia;

import java.util.HashSet;
import java.util.Set;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Simple representation of a UPnP result.
 * 
 * @author Darren Carlson
 */
public class AmbientMediaResult implements IAmbientMediaResult {
	/**
	 * Required CREATOR field that generates instances of this Parcelable class from a Parcel.
	 * 
	 * @see http://developer.android.com/reference/android/os/Parcelable.Creator.html
	 */
	public static final Parcelable.Creator<AmbientMediaResult> CREATOR = new Parcelable.Creator<AmbientMediaResult>() {
		public AmbientMediaResult createFromParcel(Parcel in) {
			return new AmbientMediaResult(in);
		}

		public AmbientMediaResult[] newArray(int size) {
			return new AmbientMediaResult[size];
		}
	};
	private boolean success = false;
	private String message = "No Message";

	public AmbientMediaResult() {
	}

	public AmbientMediaResult(boolean success, String message) {
		this.success = success;
		this.message = message;
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean isSuccess() {
		return success;
	}

	void setSuccess(boolean success) {
		this.success = success;
	}

	/**
	 * {@inheritDoc}
	 */
	public String getMessage() {
		return message;
	}

	void setMessage(String message) {
		this.message = message;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Set<String> getStringRepresentationFormats() {
		Set<String> formats = new HashSet<String>();
		formats.add("dynamix/web");
		return formats;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getStringRepresentation(String format) {
		if (format.equalsIgnoreCase("dynamix/web"))
			return "success=" + success + "&message=" + message;
		else
			return "";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getImplementingClassname() {
		return this.getClass().getName();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getContextType() {
		return "org.ambientdynamix.contextplugins.ambientmedia.interaction";
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName();
	};

	public IBinder asBinder() {
		return null;
	}

	public int describeContents() {
		return 0;
	}

	public void writeToParcel(Parcel out, int arg1) {
		out.writeByte((byte) (success ? 1 : 0));
		out.writeString(message);
	}

	private AmbientMediaResult(final Parcel in) {
		success = in.readByte() == 1;
		message = in.readString();
	}
}
