# The Ambient Media Plugin
The Ambient Media Plugin controls Ambient Media devices such as Apple Tvs or UPNP enabled media renderers.

All interaction is performed through the Ambient Control mechanisms. See [Ambient Control documentation](https://bitbucket.org/dynamixdevelopers/ambientcontrolplugin)

The plugins control description is as follows:
```JSON
{
    "name":"AmbientMedia",
    "artifact_id":"org.ambientdynamix.contextplugins.ambientmedia",
    "description":"",
    "pluginVersion":"1.0.0",
    "owner_id":"",
    "platform":"",
    "minPlatformVersion":"",
    "minFrameworkVersion":"",
    "inputList":[
        {
            "mandatoryControls":[
                "PLAYBACK_PLAY_PAUSE"
            ],
            "priority":1
        }
    ],
    "outputList":{
        "Playback Color":"DISPLAY_COLOR"
    },
    "optionalInputList":[
        "PLAYBACK_PREVIOUS",
        "PLAYBACK_STOP",
        "PLAYBACK_FORWARD_SEEK",
        "PLAYBACK_BACKWARD_SEEK",
        "PLAYBACK_NEXT",
        "DISPLAY_VIDEO",
        "DISPLAY_IMAGE"
    ]
}
```

If you don't want to use Ambient control to interact with this plugin you can use the usual context request interaction logic as follows

# Context Support
* `org.ambientdynamix.contextplugins.ambientmedia.discovery` provides details about discovered media devices.

* `org.ambientdynamix.contextplugins.ambientmedia.interaction` Send an interaction command to a device, such as play a video url

# Native App Usage
Integrate the Dynamix Framework within your app as [shown here](http://ambientdynamix.org/documentation/integration/). This guide demonstrates how to connect to Dynamix, create context handlers, register for context support, receive events, interact with plug-ins, and use callbacks. To view a simple example of using Dynamix in your native programs, see [this guide](http://ambientdynamix.org/documentation/native-app-quickstart).

## Add context support for the plug-in
```
#!java
contextHandler.addContextSupport("org.ambientdynamix.contextplugins.ambientmedia", "org.ambientdynamix.contextplugins.ambientmedia.discovery", callback, listener);
```
Once context support is successfully added, your app will automatically receive an event whenever ambient media devices are discovered. You can also query for discovered devices by sending a contextRequest:
```java
contextHandler.contextRequest("org.ambientdynamix.contextplugins.ambientmedia", "org.ambientdynamix.contextplugins.ambientmedia.discovery", callback);
```
You can also register to control ambient media devices like this:
```
#!java
contextHandler.addContextSupport("org.ambientdynamix.contextplugins.ambientmedia", "org.ambientdynamix.contextplugins.ambientmedia.interaction", callback, listener);
```

from a native app interaction can be performed like this:
#### Play a url:
```java
// Configure the request for play
Bundle config = new Bundle();
config.putString("interaction_type", "DeviceCommand");
config.putString("device_id", deviceId);
config.putString("command", "play");
config.putString("title", title); // Optional
config.putString("uri", uri);
// Call Dynamix using the config bundle
contextHandler.configuredContextRequest("org.ambientdynamix.contextplugins.ambientmedia", "org.ambientdynamix.contextplugins.ambientmedia.interaction", config);
```
#### Stop or pause playback
```java
// Configure the request for stop
Bundle config = new Bundle();
config.putString("interaction_type", "DeviceCommand");
config.putString("device_id", deviceId);
config.putString("command", "stop");//use pause for pausing
// Call Dynamix using the config bundle
contextHandler.configuredContextRequest("org.ambientdynamix.contextplugins.ambientmedia", "org.ambientdynamix.contextplugins.ambientmedia.interaction", config);
```
See [The web app documentation](http://ambientdynamix.org/documentation/dynamix-2-web-app-quickstart) to learn how to invoke the same functionality from a browser.