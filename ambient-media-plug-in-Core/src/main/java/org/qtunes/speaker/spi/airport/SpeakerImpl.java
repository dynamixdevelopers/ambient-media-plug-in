package org.qtunes.speaker.spi.airport;

import org.qtunes.speaker.*;

import java.net.*;
import java.util.*;

public class SpeakerImpl implements Speaker {

    private volatile AirtunesSpeaker speaker;
    private volatile AirtunesManagerService managerservice;

    private boolean bigEndian;
    private int holdlen, delay;
    private byte[] holdbuf = new byte[1408];
    private InetAddress host;
    private int port;
    private String displayName;


    public SpeakerImpl(String displayName, InetAddress host,int port) {
        this.displayName = displayName;
        this.port = port;
        this.host = host;
    }

    private InetAddress getHost() {
        return host;
    }
    
    public Exception getError() {
        AirtunesSpeaker tempspeaker = speaker;
        return tempspeaker==null ? null : tempspeaker.getError();
    }

    public int getDelay() {
        return delay; 
    }

    public int getBufferSize() {
        return 1408;
    }

    @Override
    public String getDisplayName() {
        return displayName;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int getUniqueId() {
        return displayName.hashCode();
    }

    public void write(byte[] buf, int off, int len) {
        if (!isOpen()) {
            throw new IllegalStateException("Not open");
        }
        if (managerservice.isListening(this)) {
            int size = 1408;
            if (holdlen != 0 && holdlen + len >= size) {
                int len1 = size - holdlen;
                System.arraycopy(buf, off, holdbuf, holdlen, len1);
                managerservice.getManager().sendAudioPacket(holdbuf, 0, size, bigEndian);
                holdlen = 0;
                off += len1;
                len -= len1;
            }
            while (len >= size) {
                managerservice.getManager().sendAudioPacket(buf, off, size, bigEndian);
                off += size;
                len -= size;
            }
            if (len != 0) {
                System.arraycopy(buf, off, holdbuf, holdlen, len);
                holdlen += len;
            }
        }
    }

    public void flush() {
        if (!isOpen()) {
            throw new IllegalStateException("Not open for");
        }
        if (managerservice.isListening(this)) {
            managerservice.getManager().clear();
        }
        holdlen = 0;
    }

    public void drain() {
        if (!isOpen()) {
            throw new IllegalStateException("Not open for");
        }
        if (managerservice.isListening(this)) {
            managerservice.getManager().drain();
        }
        holdlen = 0;
    }

    public boolean isOpen() {
        return managerservice != null;
    }

    public void close() {
        if (!isOpen()) {
            throw new IllegalStateException("Not open for");
        }
        synchronized(managerservice.getManager()) {
            managerservice.getManager().removeSpeaker(speaker);
            managerservice.remove(this);
            speaker = null;
            holdlen = 0;
            managerservice = null;
        }
    }

    public void open(float sampleRate, int sampleSizeInBits, int channels, boolean signed, boolean bigEndian) {
        if (sampleRate!=44100 || sampleSizeInBits!=16 || channels!=2 || !signed) {
            throw new IllegalArgumentException("Wrong params");
        }
        if (managerservice == null) {
            managerservice = new AirtunesManagerService();
        }
        this.bigEndian = bigEndian;
        holdlen = 0;
        synchronized(managerservice.getManager()) {
            speaker = managerservice.getManager().addSpeaker(getDisplayName(), getHost(), port, null);
            managerservice.add(this);
        }
    }

    public void setGain(float gain) {
        if (speaker != null) {
            speaker.setGain(gain);
        }
    }



    public boolean hasGain() {
        return true;
    }

    //----------------------------------------------------------------------------

    public void stopService() {
        if (isOpen()) {
            close();
        }
    }


}
