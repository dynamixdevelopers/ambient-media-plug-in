package org.qtunes.speaker.spi.airport;

import java.util.*;

public class    AirtunesManagerService{

    private ArrayList<SpeakerImpl> speakers;
    private AirtunesManager manager;

    public AirtunesManagerService() {
        speakers = new ArrayList<SpeakerImpl>();
        manager = new AirtunesManager("airtunes");
    }

    public AirtunesManager getManager() {
        return manager;
    }

    void add(SpeakerImpl speaker) {
        synchronized(speakers) {
            if (!speakers.contains(speaker)) {
                speakers.add(speaker);
            }
        }
    }

    void remove(SpeakerImpl speaker) {
        synchronized(speakers) {
            speakers.remove(speaker);
        }
    }

    boolean isListening(SpeakerImpl speaker) {
        synchronized(speakers) {
            return !speakers.isEmpty() && speaker == speakers.get(0);
        }
    }

    public Map<String,Object> reportState() {
        return null;
    }

}
