package org.qtunes.speaker;


/**
 * A Speaker is an output device, which may be a local JavaSound org.qtunes.speaker,
 * an Airtunes org.qtunes.speaker, a network org.qtunes.speaker or similar.  Writing to a Speaker should <b>never</b> throw
 * an Exception of any sort - instead the internal {#getException Exception}
 * should be set. This is reset when the Speaker is opened.
 */
public interface Speaker {

    /**
     * Return the number of ms delay for this org.qtunes.speaker, between a packet
     * being written and it being heard
     */
    public int getDelay();

    /**
     * Return the size of the org.qtunes.speaker buffer in bytes.
     */
    public int getBufferSize();

    /**
     * Return a nice name for the Speaker for display to the user
     */
    public String getDisplayName();

    /**
     * Return a unique ID for the org.qtunes.speaker
     */
    int getUniqueId();

    /**
     * Returns true if this org.qtunes.speaker can have its gain adjusted
     */
    public boolean hasGain();

    /**
     * Set the gain of this org.qtunes.speaker. A value of NaN means "no audio".
     * If this method fails, set the Error.
     */
    public void setGain(float gain);

    /**
     * If this org.qtunes.speaker has failed for some reason, return the Exception.
     * Otherwise return null.
     */
    public Exception getError();

    /**
     * Open the Speaker and start it. Reset any error that may have been set.
     */
    public void open(float sampleRate, int sampleSizeInBits, int channels, boolean signed, boolean bigEndian);

    /**
     * Write data to the org.qtunes.speaker. If it fails, set the error
     */
    public void write(byte[] buf, int off, int len);

    /**
     * Immediately stop the org.qtunes.speaker and discard any cached data written to it
     * If it fails, set the error.
     */
    public void flush();

    /**
     * Block until the data already written to the org.qtunes.speaker completes
     * If it fails, set the error.
     */
    public void drain();

    /**
     * Close the org.qtunes.speaker, immediately stopping it and discarding any data.
     * Does not throw exception or fail.
     */
    public void close();

    /**
     * True after open has been called, false after close has been called
     */
    public boolean isOpen();

}
