package org.qtunes.player;

import org.qtunes.speaker.*;
import java.util.*;
import java.io.*;

/**
 * A Player plays tracks to one or more speakers which may be local speaker
 * (probably a {SourceDataLine}), an Airtunes speaker or some other type (eg RTSP, uPnP etc.)
 * Each player maintains it's own playlist and tracks may be cued, reordered, skipped
 * and so on as you'd expect.
 */
public interface Player  {

    /**
     * Skip to the next track in the playlist. If there are no
     * more tracks in the queue, stop playing
     */
    public void playNext();

    /**
     * Set the currently playing track
     */
     public void setCurrentTrackIndex(int ix);

    /**
     * Set the player to be paused (true) or playing (false).
     */
    public void setPaused(boolean paused);

    /**
     * Return true if the player is paused, false if it's playing or
     * stopped.
     */
    public boolean isPaused();

    //-------------------------------------------------------------------------------

    /**
     * Get the collection of speakers that could be used by this Player and that
     * aren't currently in use elsewhere.
     */
    public Collection<Speaker> getAvailableSpeakers();

    /**
     * Add a Speaker to the Player. Should only be called by {Speaker#setPlayer}.
     */
    public void addSpeaker(Speaker speaker);

    /**
     * Remove a Speaker to the Player. Should only be called by {Speaker#setPlayer}.
     */
    public void removeSpeaker(Speaker speaker);

    /**
     * Return the list of {@link Speaker} objects current in use by this Player
     */
    public Collection<Speaker> getSpeakers();

    public List<File> getPlaylist();

    public void addTrack(File file);

    public void startService();

    public void stopService();

}
