//package org.qtunes.player.spi;
//
//import java.io.*;
//import java.util.*;
////import javax.sound.sampled.*;
//import javazoom.jl.decoder.*;
//
///**
// * AudioInputStream wrapper around the Bitstream.
// */
//public class JLInputStream extends AudioInputStream {
//
//    private static final boolean BIGENDIAN = true;
//    private final Decoder decoder;
//    private final Bitstream bitstream;
//    private byte[] data;
//    private int readhead, writehead, threshold, markreadhead;
//    private final boolean bigendian;
//    private boolean eof;
//
//   public static JLInputStream getInputStream(File file) throws UnsupportedAudioFileException, IOException {
//        int streamlength = (int)file.length();
//        InputStream in = new BufferedInputStream(new FileInputStream(file), 8192);
//        try {
//            Decoder decoder = new Decoder();
//            Bitstream bitstream = new Bitstream(in);
//            Header header = bitstream.readFrame();
//            if (header != null) {
//                SampleBuffer buf = (SampleBuffer)decoder.decodeFrame(header, bitstream);
//                AudioFormat format = getAudioFormat(decoder, header);
//                int numframes = getNumFrames(format, header, streamlength);
//                JLInputStream jin = new JLInputStream(decoder, in, bitstream, format, numframes);
//                jin.nextBlock(buf.getBuffer());
//                bitstream.closeFrame();
//                return jin;
//            } else {
//                throw new UnsupportedAudioFileException("No MP3 Header");
//            }
//        } catch (BitstreamException e) {
//            UnsupportedAudioFileException e2 = new UnsupportedAudioFileException("Invalid MP3");
//            e2.initCause(e);
//            throw e2;
//        } catch (DecoderException e) {
//            UnsupportedAudioFileException e2 = new UnsupportedAudioFileException("Invalid MP3");
//            e2.initCause(e);
//            throw e2;
//        }
//    }
//
//    private static AudioFormat getAudioFormat(Decoder decoder, Header header) {
//        int channels = decoder.getOutputChannels();
//        float freq = decoder.getOutputFrequency();
////        int blocksize = decoder.getOutputBlockSize() * 2;
//        int blocksize = 4;
//        float framerate = freq / blocksize / channels;
//        HashMap<String,Object> properties = new HashMap<String,Object>();
//        properties.put("bitrate", new Integer(header.bitrate()));
//        properties.put("vbr", new Boolean(header.vbr()));
//        properties.put("version", header.version_string()+"."+header.layer_string());
//        properties.put("buffersize", new Integer(decoder.getOutputBlockSize()*2));
//        return new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, freq, 16, channels, blocksize, framerate, BIGENDIAN, properties);
//    }
//
//    private static int getNumFrames(AudioFormat format, Header header, int streamlength) {
//        int numframes = AudioSystem.NOT_SPECIFIED;
//        if (streamlength > 0) {
//            float time = header.total_ms(streamlength);
//            numframes = (int)Math.ceil(time * format.getFrameRate() / 1000);
//        }
//        return numframes;
//    }
//
//    JLInputStream(Decoder decoder, InputStream in, Bitstream bitstream, AudioFormat format, int numframes) {
//        super(in, format, numframes);
//        this.decoder = new Decoder();
//        this.bitstream = bitstream;
//        this.bigendian = format.isBigEndian();
//        this.writehead = -1;
//        this.markreadhead = -1;
//        this.threshold = 1;
//    }
//
//    private boolean nextBlock() throws IOException {
//        if (eof) return false;
//        try {
//            Header header = bitstream.readFrame();
//            if (header!=null) {
//                SampleBuffer buf = (SampleBuffer)decoder.decodeFrame(header, bitstream);
//                nextBlock(buf.getBuffer());
//                bitstream.closeFrame();
//                return true;
//            } else {
//                eof = true;
//                return false;
//            }
//        } catch (BitstreamException e) {
//            IOException e2 = new IOException("Exception reading MP3");
//            e2.initCause(e);
//            throw e2;
//        } catch (DecoderException e) {
//            IOException e2 = new IOException("Exception reading MP3");
//            e2.initCause(e);
//            throw e2;
//        }
//    }
//
//    void nextBlock(short[] samples) {
//        if (data==null) {
//            threshold = decoder.getOutputBlockSize() * 2;
//            data = new byte[threshold * 2];
//        }
//        if (samples.length * 2 > availableForWriting()) {
//            throw new IllegalStateException("Can't read "+(samples.length*2)+" bytes, only "+availableForWriting()+" available");
//        }
//        if (writehead == -1) {
//            writehead = 0;
//        }
//
//        int oldwritehead = writehead;
//        for (int i=0;i<samples.length;i++) {
//            if (bigendian) {
//                data[writehead++] = (byte)(samples[i] >>> 8);
//                data[writehead++] = (byte)samples[i];
//            } else {
//                data[writehead++] = (byte)samples[i];
//                data[writehead++] = (byte)(samples[i] >>> 8);
//            }
//            if (writehead==data.length) {
//                writehead = 0;
//            }
//        }
//        if ((oldwritehead < readhead && (writehead > readhead || writehead < oldwritehead)) || (oldwritehead > readhead && writehead > readhead)) {
//            markreadhead = -1;
//        }
//    }
//
//    public int read(byte[] buf) throws IOException {
//        return read(buf, 0, buf.length);
//    }
//
//    public int read(byte[] buf, int off, int len) throws IOException {
//        while (!eof && availableForWriting() > threshold) {
//            nextBlock();
//        }
//        if (len==0) {
//            return 0;
//        } else if (writehead==-1) {
//            return -1;
//        }
//        int numread = 0;
//        if (readhead >= writehead) {
//            int numtoread = Math.min(len, data.length - readhead);
//            System.arraycopy(data, readhead, buf, off, numtoread);
//            numread += numtoread;
//            off += numtoread;
//            len -= numtoread;
//            readhead += numtoread;
//            if (readhead==data.length) {
//                readhead = 0;
//            }
//        }
//        if (readhead < writehead) {
//            int numtoread = Math.min(len, writehead - readhead);
//            if (numtoread > 0) {
//                System.arraycopy(data, readhead, buf, off, numtoread);
//                numread += numtoread;
//                readhead += numtoread;
//            }
//        }
//        if (readhead==writehead) {
//            writehead = -1;
//            readhead = 0;
//        }
//        return numread;
//    }
//
//    public int read() throws IOException {
//        while (!eof && availableForWriting() > threshold) {
//            nextBlock();
//        }
//        if (writehead==-1) {
//            return -1;
//        } else {
//            int val = data[readhead++] & 0xFF;
//            if (readhead==data.length) {
//                readhead = 0;
//            }
//            if (readhead==writehead) {
//                writehead = -1;
//                readhead = 0;
//            }
//            return val;
//        }
//    }
//
//    public void mark(int limit) {
//        markreadhead = readhead;
//    }
//
//    public boolean markSupported() {
//        return true;
//    }
//
//    public void reset() throws IOException {
//        if (markreadhead==-1) {
//            throw new IOException("Invalid mark");
//        } else {
//            readhead = markreadhead;
//        }
//    }
//
//    public int available() {
//        if (writehead==-1) {
//            return 0;
//        } else if (writehead > readhead) {
//            return writehead - readhead;
//        } else {
//            return (data.length - readhead) + writehead;
//        }
//    }
//
//    private int availableForWriting() {
//        if (writehead==-1) {
//            return data.length;
//        } else if (writehead > readhead) {
//            return (data.length - writehead) + readhead;
//        } else {
//            return readhead - writehead;
//        }
//    }
//
//    public long skip(long amount) throws IOException {
//        int avail = available();
//        long origamount = amount;
//        if (avail >= 0) {
//            readhead = (readhead + avail) % data.length;
//            amount -= avail;
//            if (readhead==writehead) {
//                readhead = 0;
//                writehead = -1;
//            }
//        }
//        while (amount > 0) {
//            try {
//                Header header = bitstream.readFrame();
//                if (header!=null) {
//                    int framebytes = 1152 * (header.mode()==Header.SINGLE_CHANNEL ? 2 : 4);
//                    if (framebytes > amount) {
//                        SampleBuffer buf = (SampleBuffer)decoder.decodeFrame(header, bitstream);
//                        nextBlock(buf.getBuffer());
//                        readhead += amount;
//                        amount = 0;
//                    } else {
//                        amount -= framebytes;
//                    }
//                    bitstream.closeFrame();
//                } else {
//                    break;
//                }
//            } catch (BitstreamException e) {
//                IOException e2 = new IOException("Exception reading MP3");
//                e2.initCause(e);
//                throw e2;
//            } catch (DecoderException e) {
//                IOException e2 = new IOException("Exception reading MP3");
//                e2.initCause(e);
//                throw e2;
//            }
//        }
//        return origamount - amount;
//    }
//
//}
