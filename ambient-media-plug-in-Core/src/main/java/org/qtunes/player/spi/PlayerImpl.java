package org.qtunes.player.spi;


import android.util.Log;
import javazoom.jl.decoder.*;
import org.qtunes.player.*;
import org.qtunes.speaker.*;
import sun.misc.IOUtils;

import java.util.*;
import java.io.*;
//import javax.sound.sampled.*;

/**
 * A Player plays tracks to one or more speakers (technically a {SourceDataLine}
 * which may be a local speaker, an Airtunes speaker or some other type (eg RTSP, uPnP etc.)
 * Each player maintains it's own playlist and tracks may be cued, reordered, skipped
 * and so on as you'd expect.
 */
public class PlayerImpl implements Player, Runnable {

    private static final int BUFSIZE = 4096;
    private final String TAG = this.getClass().getSimpleName();
    private int revision;
    private List<File> queue;
    private volatile boolean paused, cancelled, trackinterrupted;
    private volatile TrackSession tracksession;
    private MultiLine multiline;
    private Thread thread;
    private volatile int volume;

    public PlayerImpl() {
        queue = new Vector<File>();
        multiline = new MultiLine(this);
    }
    /**
     * The main play thread. Should be bulletproof
     */
    public void run() {
        byte[] buf = new byte[BUFSIZE];
//        AudioInputStream in = null;
        ByteArrayInputStream bain = null;
        TrackSession lastplaying = null, nowplaying = null;
        int streamoffset = 0;

        boolean autoadvance = false;
        while (!cancelled) {
            synchronized(this) {
                trackinterrupted = false;
                lastplaying = nowplaying;
                if(queue.size()>0){
                    tracksession = nowplaying = new TrackSession(queue.get(0));
                    queue.remove(0);
                }

            }
            if (nowplaying == null) {
//                context.debug("Player wakes: now==null last"+(lastplaying==null?"==null":"!=null"));
                if (lastplaying != null) {
                    if (autoadvance) {
                        multiline.drain();
                    } else {
                        multiline.flush();
                    }
                    multiline.close();
                    lastplaying = null;
                }
                autoadvance = false;
                try {
                    synchronized(this) {
                        wait();
                    }
                } catch (InterruptedException e) {}
            } else {
                Log.i(TAG, "starting playback of Track: " + tracksession.getTrack().getName());
                autoadvance = false;
                try {
                    bain =  tracksession.getStream(0,60000);
                    streamoffset = 0;
//                    nowplaying.setFormat(bain.getFormat());

//                    if (lastplaying!=null && !lastplaying.formatMatches(nowplaying)) {
//                        multiline.drain();
//                        multiline.close();
//                        lastplaying = null;
//                    }
                    if (lastplaying == null) {
                        Log.i(TAG, "opening Speaker connection");
                        multiline.open(tracksession.getBpms(), 16, 2, true, true);
                    }

                    while (!trackinterrupted && !cancelled) {
                        if (bain!=null && streamoffset > nowplaying.tell()) {
                            bain.close();
                            bain = null;
                        }
                        if (bain==null) {
                            bain =  nowplaying.getStream(0,5000);
                            streamoffset = 0;
                        }
                        int n;
                        while (nowplaying.tell() > streamoffset && (n=bain.read(buf, 0, Math.min(nowplaying.tell() - streamoffset, buf.length)))>=0) {
                            streamoffset += n;
                        }
                        if (paused) {
                            Log.i(TAG, "Player paused");
                            multiline.stop();
                            try {
                                synchronized(this) {
                                    wait();
                                }
                            } catch (InterruptedException e) {}
                        }
                        if (!paused) {
                            if (!multiline.hasSpeakers()) {
                                trackinterrupted = true;
                            } else if ((n = bain.read(buf, 0, buf.length)) >= 0) {
                                streamoffset += n;
                                nowplaying.advance(n);
                                multiline.write(buf, 0, n);
                            } else {
                                Log.i(TAG, "Player ends track");
                                trackinterrupted = autoadvance = true;
                            }
                        }
                    }
                    Log.i(TAG, "Player ends loop: int="+trackinterrupted+" auto="+autoadvance+" cancelled="+cancelled);
                    if (trackinterrupted) {
                        if (!autoadvance) {
                            multiline.flush();
                        }
                    }
                } catch (Exception e) {
                    Log.i(TAG, e.getMessage());
                    if (bain!=null) {
                        try { bain.close(); } catch (Exception e2) {Log.i(TAG, e2.getMessage());}
                        bain = null;
                    }
                    if (multiline.isOpen()) {
                        try {
                            multiline.flush();
                            multiline.close();
                        } catch (Exception e2) {Log.i(TAG, e2.getMessage());}
                    }
                    lastplaying = null;
                    autoadvance = true;
                }
            }
        }
        Log.i(TAG, "Player dies");
    }

    synchronized void interrupt() {
        trackinterrupted = true;
        notifyAll();
    }

    //----------------------------------------------------------------------------------

    public synchronized void playNext() {
//        tracksession = new TrackSession(queue.get(0));
//        queue.remove(0);
        interrupt();
    }

    public synchronized void setCurrentTrackIndex(int ix) {
        tracksession = new TrackSession(queue.get(ix));
        for(int i=0;i<ix;i++)
            queue.remove(i);
        interrupt();
    }

    public synchronized void stop() {
        tracksession = null;
        interrupt();
    }

    public synchronized void seek(int ms) {
        if (tracksession != null) {
            tracksession.seekMS(ms);
        }
    }

    public synchronized void setPaused(boolean paused) {
        if (this.paused != paused && tracksession != null) {
            this.paused = paused;
            notifyAll();
        }
    }

    public boolean isPaused() {
        return paused;
    }

    public int getTrackPosition() {
        return Math.max(0, tracksession==null ? 0 : tracksession.tellMS() - multiline.getDelay());
    }

    /**
     * Return the currently playing Track, or null if no track is playing
     */
    public File getCurrentTrack() {
        return tracksession == null ? null : tracksession.track;
    }

    public int getVolume() {
        return volume;
    }

    public int getRevision() {
        return revision;
    }

    /**
     * Return the Playlist
     */
    public List<File> getPlaylist() {
        return queue;
    }

    @Override
    public void addTrack(File file) {
        queue.add(file);
    }


    //-------------------------------------------------------------------------------

    public Collection<Speaker> getAvailableSpeakers() {
        return multiline.getSpeakers();
    }

    public void addSpeaker(Speaker speaker) {
        Collection<Speaker> speakers = multiline.getSpeakers();
        if (speakers.add(speaker)) {
            multiline.setSpeakers(speakers);
        }
    }

    public void removeSpeaker(Speaker speaker) {
        Collection<Speaker> speakers = multiline.getSpeakers();
        if (speakers.remove(speaker)) {
            multiline.setSpeakers(speakers);
        }
    }

    public Collection<Speaker> getSpeakers() {
        return multiline.getSpeakers();
    }


    //-----------------------------------------------------------------------------

    public void startService() {
        this.cancelled = false;
        this.tracksession = null;
        volume = 50;
        thread = new Thread(this, "Dynamix Player Thread");
        thread.start();
    }

    public void stopService() {
        cancelled = trackinterrupted = true;
        tracksession = null;
        thread.interrupt();
        try {
            thread.join();
        } catch (InterruptedException e) { }
        multiline = null;
    }







}
