package org.qtunes.player.spi;

import android.util.Log;
import javazoom.jl.decoder.*;

import javax.sound.sampled.AudioFormat;
import java.io.*;

class TrackSession {

    private final String TAG = this.getClass().getSimpleName();
    final File track;

    private volatile int offset;
//    private AudioFormat format;
    private float bpms = -1;         // bytes per MS


    TrackSession(File track) {
        this.track = track;
    }

//    AudioFormat getFormat() {
//        return format;
//    }

//    boolean formatMatches(TrackSession session) {
//        return session!=null && session.format.matches(format);
//    }

//    void setFormat(AudioFormat format) {
//        if (this.format!=null && !this.format.matches(format)) {
//            throw new Error();
//        }
//        this.format = format;
//        this.bpms = format.getSampleRate() * format.getSampleSizeInBits() * format.getChannels() / 8000;
//        this.offset = 0;
//    }

    public String toString() {
        return "[track=" + track.toString()+" offset="+offset+"]";
    }

    void advance(int delta) {
        this.offset += delta;
    }

    void seek(int offset) {
        this.offset = offset;
    }

    int tell() {
        return offset;
    }

    void seekMS(int ms) {
        int bytes = (int)(ms * bpms) & ~3;
//        int length = (int)(track.get(Field.Duration) * bpms) & ~3;
//        if (bytes >= 0 && bytes <= length) {
//            seek(bytes);
//        }
        throw new RuntimeException("not implememted");
    }

    int tellMS() {
        return (int)(offset / bpms);
    }

    public File getTrack() {
        return track;
    }

    public byte[] decode(File file, int startMs, int maxMs)
            throws IOException {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream(1024);

        float totalMs = 0;
        boolean seeking = true;

        InputStream inputStream = new BufferedInputStream(new FileInputStream(file), 8 * 1024);
        try {
            Bitstream bitstream = new Bitstream(inputStream);
            Decoder decoder = new Decoder();

            boolean done = false;
            while (! done) {
                Header frameHeader = bitstream.readFrame();
                if (frameHeader == null) {
                    done = true;
                } else {
                    totalMs += frameHeader.ms_per_frame();

                    if (totalMs >= startMs) {
                        seeking = false;
                    }

                    if (! seeking) {
                        SampleBuffer output = (SampleBuffer) decoder.decodeFrame(frameHeader, bitstream);

                        if (output.getSampleFrequency() != 44100
                                || output.getChannelCount() != 2) {
                            throw new RuntimeException("mono or non-44100 MP3 not supported");
                        }

                        short[] pcm = output.getBuffer();
                        for (short s : pcm) {
                            outStream.write(s & 0xff);
                            outStream.write((s >> 8 ) & 0xff);
                        }
                    }

                    if (totalMs >= (startMs + maxMs)) {
                        done = true;
                    }
                }
                bitstream.closeFrame();
            }

            bpms = decoder.getOutputFrequency();
            return outStream.toByteArray();

        } catch (BitstreamException e) {
            throw new IOException("Bitstream error: " + e);
        } catch (DecoderException e) {
            Log.w(TAG, "Decoder error", e);
            throw new RuntimeException("Decoder error",e);
        } finally {
            outStream.close();
        }
    }

    public ByteArrayInputStream getStream(int startMs, int maxMs) throws IOException {
        return new ByteArrayInputStream(decode(track,startMs,maxMs));
    }

    float getBpms() {
        if(bpms==-1)
            throw new RuntimeException("no call to getStream yet, unknown sample rate");
        return bpms;
    }
}
