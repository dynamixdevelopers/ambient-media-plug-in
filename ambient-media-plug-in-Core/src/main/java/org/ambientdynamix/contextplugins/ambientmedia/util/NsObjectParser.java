package org.ambientdynamix.contextplugins.ambientmedia.util;

import android.util.Log;
import com.dd.plist.NSDictionary;
import com.dd.plist.NSNumber;
import com.dd.plist.NSObject;
import com.dd.plist.PropertyListParser;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;

/**
 * Created by workshop on 12/17/13.
 */
public class NsObjectParser {

    private String result;
    private NSDictionary rootDict;

    public NsObjectParser(String result) throws Exception {
        this.result = result;
        rootDict = (NSDictionary) PropertyListParser.parse(new ByteArrayInputStream(result.getBytes("UTF-8")));
    }

    public double getDouble(String key){

        double value = Double.MIN_VALUE;
//        Log.i("NSParser", "contains key: " + key + ": " + rootDict.containsKey(key));
        NSObject nsValue = rootDict.objectForKey(key);
        if (nsValue != null && nsValue.getClass().equals(NSNumber.class)) {
            NSNumber num = (NSNumber) nsValue;
//            Log.i("NSParser", "value of type: " + num.type());
            if (num.type() == NSNumber.REAL){
                value = num.doubleValue();
            }else if(num.type() == NSNumber.INTEGER)
                value = num.intValue();
        }
            return value;
    }

    public boolean getBoolean(String key) {
        boolean value = false;
//        Log.i("NSParser", "contains key: " + key + ": " + rootDict.containsKey(key));
        NSObject nsValue = rootDict.objectForKey(key);
        if (nsValue != null && nsValue.getClass().equals(NSNumber.class)) {
            NSNumber num = (NSNumber) nsValue;
//            Log.i("NSParser", "value of type: " + num.type());
            if (num.type() == NSNumber.BOOLEAN){
                value = num.boolValue();
            }
        }
        return value;
    }
}
