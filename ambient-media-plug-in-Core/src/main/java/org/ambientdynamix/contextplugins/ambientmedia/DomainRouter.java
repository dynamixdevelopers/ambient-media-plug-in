/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.ambientmedia;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Maintains a list of discovered device IDs and their mapped controllers. Use to map incoming request IDs to the
 * correct controller.
 * @author Darren Carlson
 *
 */
public class DomainRouter {
	private static Map<String, AmbientMediaDevice> devices = new ConcurrentHashMap<String, AmbientMediaDevice>();

	public static boolean addDevice(AmbientMediaDevice d) {
		if (!devices.containsKey(d.getId())) {
			devices.put(d.getId(), d);
		}
		return true;
	}

	public static boolean removeDevice(AmbientMediaDevice d) {
		return devices.remove(d.getId()) != null ? true : false;
	}

	public static boolean containsDevice(String id) {
		return devices.containsKey(id);
	}

	public static AmbientMediaDevice getDevice(String id) {
		return devices.get(id);
	}

	public static void clearAllDevices() {
		devices.clear();
	}

	public static Collection<AmbientMediaDevice> getAllDevices() {
		return devices.values();
	}
}
