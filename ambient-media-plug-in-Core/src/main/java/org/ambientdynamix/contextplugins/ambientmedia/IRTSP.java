package org.ambientdynamix.contextplugins.ambientmedia;

import java.util.UUID;

// http://tools.ietf.org/html/rfc2326
/*
 *  method            direction        object     requirement
 DESCRIBE          C->S             P,S        recommended
 ANNOUNCE          C->S, S->C       P,S        optional
 GET_PARAMETER     C->S, S->C       P,S        optional
 OPTIONS           C->S, S->C       P,S        required
 (S->C: optional)
 PAUSE             C->S             P,S        recommended
 PLAY              C->S             P,S        required
 RECORD            C->S             P,S        optional
 REDIRECT          S->C             P,S        optional
 SETUP             C->S             S          required
 SET_PARAMETER     C->S, S->C       P,S        optional
 TEARDOWN          C->S             P,S        required
 */
public interface IRTSP {
	//void options();

	void setup();

	void play(UUID requestId, String url, String title);

	void pause(UUID requestId);

	void teardown(UUID requestId);



}
