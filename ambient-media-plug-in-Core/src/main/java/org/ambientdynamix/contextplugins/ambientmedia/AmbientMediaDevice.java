package org.ambientdynamix.contextplugins.ambientmedia;

import java.util.UUID;

public abstract class AmbientMediaDevice implements IRTSP {
    protected String id;
	protected String displayName;
    protected String type;
    protected AmbientMediaRuntime runtime;

	public AmbientMediaDevice(String id, String displayName, String type) {
		this.id = id;
		this.displayName = displayName;
		this.type = type;
	}

	/**
	 * Returns the unique id of the device.
	 */
	public String getId() {
		return id;
	}

	/**
	 * Returns the display name of the device.
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * Returns the type of the device: MediaRenderer, MediaServer, etc.
	 */
	public String getType() {
		return type;
	}

    public void setRuntime(AmbientMediaRuntime runtime) {
        this.runtime = runtime;
    }

    public abstract void forwardSeek(UUID requestId);

    public abstract void backwardSeek(UUID requestId);

    public abstract void stop(UUID requestId);

}
