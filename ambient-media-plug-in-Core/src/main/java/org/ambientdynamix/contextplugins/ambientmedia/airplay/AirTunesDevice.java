package org.ambientdynamix.contextplugins.ambientmedia.airplay;

import android.os.Environment;
import org.ambientdynamix.contextplugins.ambientmedia.AmbientMediaDevice;
import org.qtunes.player.Player;
import org.qtunes.player.spi.PlayerImpl;
import org.qtunes.speaker.spi.airport.AirtunesManagerService;
import org.qtunes.speaker.spi.airport.SpeakerImpl;
import org.qtunes.speaker.Speaker;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;


import javax.jmdns.ServiceInfo;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.InetAddress;
import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * User: workshop
 * Date: 11/28/13
 * Time: 5:24 PM
 * To change this template use File | Settings | File Templates.
 */
public class AirTunesDevice extends AmbientMediaDevice {
    private Speaker speaker;
    private InetAddress host;
    private int port;
    private ServiceInfo serviceInfo;
    Player player;

    public AirTunesDevice(ServiceInfo serviceInfo,String id, String displayName, String type, InetAddress host, int port) {
        super(id, displayName, type);
        this.host = host;
        this.port = port;
        this.serviceInfo = serviceInfo;
    }

    @Override
    public void setup() {
        player = new PlayerImpl();
        speaker = new SpeakerImpl(displayName,host,port);
        player.addSpeaker(speaker);
    }

    @Override
    public void play(UUID requestId, String url, String title) {
        try {
            setup();
            player.addTrack(getFileFromPath(url));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        player.startService();
//        player.playNext();
    }

    public File getFileFromPath(String Path)throws FileNotFoundException{
        if(isExternalMediaAvailable()){
            File root = android.os.Environment.getExternalStorageDirectory();
            File audioFile;
            if(Path.startsWith(root.getAbsolutePath()))
                audioFile = new File(Path);
            else
                audioFile = new File(root.getAbsolutePath()+ Path);
            if(audioFile.getAbsolutePath().endsWith("mp3"))
                return audioFile;
            else
                throw new FileNotFoundException("File doesn't seem to be an mp3");

        }
        throw new FileNotFoundException("no external Storage available");
    }

    @Override
    public void pause(UUID requestId) {
        player.setPaused(true);
    }

    @Override
    public void teardown(UUID requestId) {
        speaker.close();
        player.removeSpeaker(speaker);
        player.setPaused(true);
        player.stopService();
    }

    /** Method to check whether external media available and writable. This is adapted from
     http://developer.android.com/guide/topics/data/data-storage.html#filesExternal */

    private boolean isExternalMediaAvailable(){
        boolean mExternalStorageAvailable = false;
        boolean mExternalStorageWriteable = false;
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // Can read and write the media
            mExternalStorageAvailable = mExternalStorageWriteable = true;
        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            // Can only read the media
            mExternalStorageAvailable = true;
            mExternalStorageWriteable = false;
        } else {
            // Can't read or write
            mExternalStorageAvailable = mExternalStorageWriteable = false;
        }
        return mExternalStorageAvailable;
    }

    @Override
    public void forwardSeek(UUID requestId) {
        throw new NotImplementedException();
    }

    @Override
    public void backwardSeek(UUID requestId) {
        throw new NotImplementedException();
    }

    @Override
    public void stop(UUID requestId) {
        throw new NotImplementedException();
    }
}
