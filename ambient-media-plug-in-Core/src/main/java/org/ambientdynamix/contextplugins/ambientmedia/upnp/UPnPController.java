package org.ambientdynamix.contextplugins.ambientmedia.upnp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

import org.ambientdynamix.contextplugins.ambientmedia.AmbientMediaDevice;
import org.ambientdynamix.contextplugins.ambientmedia.AmbientMediaRuntime;
import org.ambientdynamix.contextplugins.ambientmedia.BaseMediaController;
import org.teleal.cling.UpnpService;
import org.teleal.cling.UpnpServiceConfiguration;
import org.teleal.cling.UpnpServiceImpl;
import org.teleal.cling.android.AndroidUpnpServiceConfiguration;
import org.teleal.cling.android.AndroidWifiSwitchableRouter;
import org.teleal.cling.model.ModelUtil;
import org.teleal.cling.model.meta.Device;
import org.teleal.cling.model.meta.LocalDevice;
import org.teleal.cling.model.meta.RemoteDevice;
import org.teleal.cling.protocol.ProtocolFactory;
import org.teleal.cling.registry.DefaultRegistryListener;
import org.teleal.cling.registry.Registry;
import org.teleal.cling.transport.Router;

import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.util.Log;

public class UPnPController extends BaseMediaController {
	private final String TAG = this.getClass().getSimpleName();
	private Context context;
	private WifiManager wifiManager;
	private ConnectivityManager connectivityManager;
	private String didlTemplate;
	private UpnpService upnpService;
	private AmbientMediaRuntime runtime;
	private BrowseRegistryListener registryListener = new BrowseRegistryListener();

	public UPnPController(Context context, AmbientMediaRuntime runtime,
			WifiManager wifiManager, ConnectivityManager connectivityManager) {
		this.context = context;
		this.wifiManager = wifiManager;
		this.connectivityManager = connectivityManager;
		this.runtime = runtime;
	}

	@Override
	public void init() throws Exception {
		// Init MD5
		initMD5();
		// Setup the didlTemplate
		ClassLoader classLoader = context.getClassLoader();
		if (classLoader != null) {
			// Create a URL to the meta.xml resource
			URL url = this.getClass().getClassLoader().getResource("org/ambientdynamix/contextplugins/ambientmedia/upnp/meta.xml");
			if (url != null) {
				StringBuilder inputStringBuilder = new StringBuilder();
				BufferedReader bufferedReader = new BufferedReader(
						new InputStreamReader(url.openStream(), "UTF-8"));
				String line = bufferedReader.readLine();
				while (line != null) {
					inputStringBuilder.append(line);
					line = bufferedReader.readLine();
				}
				didlTemplate = inputStringBuilder.toString();
				bufferedReader.close();
			} else {
				throw new Exception("Could not find meta.xml resource");
			}
		} else {
			throw new Exception("Could not access classloader");
		}
	}

	@Override
	public void start() {
		// Check for the upnpService
		if (upnpService == null) {
			// Create the upnpService
			upnpService = new UpnpServiceImpl(createConfiguration(wifiManager)) {
				@Override
				protected Router createRouter(ProtocolFactory protocolFactory,
						Registry registry) {
					AndroidWifiSwitchableRouter router = UPnPController.this
							.createRouter(getConfiguration(), protocolFactory,
									wifiManager, connectivityManager);
					if (!ModelUtil.ANDROID_EMULATOR
							&& isListeningForConnectivityChanges()) {
						// Only register for network connectivity changes if we
						// are not running on emulator
						context.registerReceiver(router.getBroadcastReceiver(),
								new IntentFilter(
										"android.net.conn.CONNECTIVITY_CHANGE"));
					}
					Log.d(TAG, "Created Transport Router... Started!");
					return router;
				}
			};
		} else
			Log.w(TAG, "UPnP Service was not null, not recreating");
		// Get ready for future device advertisements
		upnpService.getRegistry().addListener(registryListener);
		// Now add all devices to the list we already know about
		for (Device device : upnpService.getRegistry().getDevices()) {
			registryListener.deviceAdded(device);
		}
		searchForDevices();
	}

	@Override
	public void stop() {
		// Shutdown the UPnPService
		if (upnpService != null) {
			if (!ModelUtil.ANDROID_EMULATOR
					&& isListeningForConnectivityChanges())
				context.unregisterReceiver(((AndroidWifiSwitchableRouter) upnpService
						.getRouter()).getBroadcastReceiver());
			Log.d(TAG, "Shutting down UPnP");
			upnpService.shutdown();
			upnpService = null;
		}
	}

	@Override
	public void close() {
		stop();
	}

	private boolean isListeningForConnectivityChanges() {
		return true;
	}

	private AndroidUpnpServiceConfiguration createConfiguration(
			WifiManager wifiManager) {
		return new AndroidUpnpServiceConfiguration(wifiManager);
	}

	private AndroidWifiSwitchableRouter createRouter(
			UpnpServiceConfiguration configuration,
			ProtocolFactory protocolFactory, WifiManager wifiManager,
			ConnectivityManager connectivityManager) {
		return new AndroidWifiSwitchableRouter(configuration, protocolFactory,
				wifiManager, connectivityManager);
	}

	private class BrowseRegistryListener extends DefaultRegistryListener {
		/* Discovery performance optimization for very slow Android devices! */
		@Override
		public void remoteDeviceDiscoveryStarted(Registry registry,
				RemoteDevice device) {
			deviceAdded(device);
		}

		@Override
		public void remoteDeviceDiscoveryFailed(Registry registry,
				final RemoteDevice device, final Exception ex) {
			Log.w(TAG, "Discovery failed for " + device.getDisplayString()
					+ " with error " + ex.toString());
			deviceRemoved(device);
		}

		/*
		 * End of optimization, you can remove the whole block if your Android
		 * handset is fast (>= 600 Mhz)
		 */
		@Override
		public void remoteDeviceAdded(Registry registry, RemoteDevice device) {
			deviceAdded(device);
		}

		@Override
		public void remoteDeviceRemoved(Registry registry, RemoteDevice device) {
			deviceRemoved(device);
		}

		@Override
		public void localDeviceAdded(Registry registry, LocalDevice device) {
			deviceAdded(device);
		}

		@Override
		public void localDeviceRemoved(Registry registry, LocalDevice device) {
			deviceRemoved(device);
		}

		public void deviceAdded(final Device device) {
			if (device.isFullyHydrated()) {
				String deviceName = device.getDetails().getFriendlyName()
						+ ": " + device.getDisplayString();
				Log.i(TAG, "deviceAdded: " + deviceName + " type "
						+ device.getType().getType() + " id "
						+ device.getIdentity().getUdn().getIdentifierString());
				// Add the device
				StringBuilder name = new StringBuilder();
				if(device.getType()
								.getType().contains("MediaRenderer"))
					name.append("[UPnP] " + device.getDetails().getFriendlyName());
				else
					name.append(device.getDetails().getFriendlyName());
				AmbientMediaDevice ad = new UPnPDevice(runtime, device, upnpService, didlTemplate,
						createId(device),name.toString(), device.getType()
								.getType());
				notifyDeviceDiscovered(ad);
			}
		}

		public void deviceRemoved(final Device device) {
			String deviceName = device.getDetails().getFriendlyName() + ": "
					+ device.getDisplayString();
			Log.i(TAG, "deviceRemoved: " + deviceName + " type "
					+ device.getType().getType() + " id "
					+ device.getIdentity().getUdn().getIdentifierString());
			// Remove the device
			notifyDeviceLost(createId(device));
		}
	}

	@Override
	public void searchForDevices() {
		if (upnpService != null) {
			Log.i(TAG, "searchForDevices");
			upnpService.getControlPoint().search();
		}
	}

	private String createId(Device d) {
		return md5(d.getIdentity().getUdn().toString());
	}
}
