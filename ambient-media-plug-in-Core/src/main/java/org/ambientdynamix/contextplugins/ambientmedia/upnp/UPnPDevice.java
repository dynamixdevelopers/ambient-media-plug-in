package org.ambientdynamix.contextplugins.ambientmedia.upnp;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.Map;
import java.util.UUID;

import org.ambientdynamix.api.application.ErrorCodes;
import org.ambientdynamix.api.contextplugin.security.PrivacyRiskLevel;
import org.ambientdynamix.api.contextplugin.security.SecuredContextInfo;
import org.ambientdynamix.contextplugins.ambientmedia.AmbientMediaDevice;
import org.ambientdynamix.contextplugins.ambientmedia.AmbientMediaResult;
import org.ambientdynamix.contextplugins.ambientmedia.AmbientMediaRuntime;
import org.ambientdynamix.contextplugins.ambientmedia.util.YoutubeUrlParser;
import org.teleal.cling.UpnpService;
import org.teleal.cling.controlpoint.ActionCallback;
import org.teleal.cling.model.action.ActionArgumentValue;
import org.teleal.cling.model.action.ActionInvocation;
import org.teleal.cling.model.message.UpnpResponse;
import org.teleal.cling.model.meta.Action;
import org.teleal.cling.model.meta.Device;
import org.teleal.cling.model.meta.Service;
import org.teleal.cling.model.types.UDAServiceId;
import org.teleal.cling.model.types.UnsignedIntegerFourBytes;

import android.util.Log;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class UPnPDevice extends AmbientMediaDevice {
	private final String TAG = this.getClass().getSimpleName();
	private static final String TITLE_PLACEHOLDER = "TITLE_PLACEHOLDER";
	private static final String MEDIA_URL_PLACEHOLDER = "MEDIA_URL_PLACEHOLDER";
	private Device device;
	private UpnpService upnpService;
	private String didlTemplate;

	public UPnPDevice(AmbientMediaRuntime runtime, Device device, UpnpService upnpService, String didlTemplate,
			String id, String displayName, String type) {
		super(id, displayName, type);
		this.didlTemplate = didlTemplate;
		this.device = device;
		this.runtime = runtime;
		this.upnpService = upnpService;
	}

	@Override
	public void setup() {
		// TODO Auto-generated method stub
	}

	@Override
	public void play(final UUID requestId, final String url, final String title) {


        if(url.contains("youtube")){
            try {
                new YoutubeUrlParser(url).getQueryString(new YoutubeUrlParser.CallbackOnURLReady(){

                    @Override
                    public void callback(Map<String, String> atvSupportedUrls) {
                        String contentLocation = atvSupportedUrls.containsValue("22") ?
                                atvSupportedUrls.get("22") : (
                                atvSupportedUrls.containsValue("18") ? atvSupportedUrls.get("18") : null);
                        if (contentLocation == null && atvSupportedUrls.size() > 0)
                            contentLocation = atvSupportedUrls.get(atvSupportedUrls.keySet().iterator().next());
                        playUrl(requestId,contentLocation,title);
                    }
                });
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }else
            playUrl(requestId,url,title);


	}
    private void playUrl(UUID requestId, String url, String title){
        // Prep metadata
        String meta = new String(didlTemplate);
        Log.i(TAG, "Updating request title = " + title + " and url = " + url);
        meta = meta.replace(TITLE_PLACEHOLDER, title);
        meta = meta.replace(MEDIA_URL_PLACEHOLDER, url);
        try {
			/*
			 * Determine media control type. I'm cheating here, since I'm not doing a
			 * complete scan of the available services and associated actions. I'm simply
			 * checking for AVTransport support, which is available for all devices in the
			 * Percom demo this plug-in was originally designed for.
			 */
            Service transportService = device.findService(new UDAServiceId("AVTransport"));
            if (transportService != null) {
                doPlayWebUrlAVTransport(requestId, device, transportService, title, url,
                        meta);
            } else {
                runtime.sendContextRequestError(requestId, "Media Playback not supported for device: "
                        + device.getDisplayString(), ErrorCodes.NOT_SUPPORTED);
            }
        } catch (Exception e) {
            runtime.sendContextRequestError(requestId, e.toString(), ErrorCodes.INTERNAL_PLUG_IN_ERROR);
        }
    }

	@Override
	public void pause(UUID requestId) {
	}

	@Override
	public void teardown(final UUID requestId) {
		try {
			doStopPlayback(requestId, device, new Runnable() {
				@Override
				public void run() {
					runtime.sendContextEvent(requestId, new SecuredContextInfo(new AmbientMediaResult(
							true, "Successfully called Stop!"), PrivacyRiskLevel.LOW));
				}
			});
		} catch (Exception e) {
			runtime.sendContextRequestError(requestId, e.toString(), ErrorCodes.INTERNAL_PLUG_IN_ERROR);
		}
	}

    @Override
    public void forwardSeek(UUID requestId) {
        throw new NotImplementedException();
    }

    @Override
    public void backwardSeek(UUID requestId) {
        throw new NotImplementedException();
    }

    @Override
    public void stop(UUID requestId) {
        throw new NotImplementedException();
    }

    private interface ICallback {
		public void execute(Object... args);
	}

	/*
	 * The problem is that, if the player is in state PLAYING, calls to 'SetAVTransportURI' go unanswered for some
	 * reason. If Stop is called on the player, calls work fine again.
	 */
	private void doPlayWebUrlAVTransport(final UUID requestId, final Device device, final Service service,
			final String title, final String uri, final String meta) throws Exception {
		Log.i(TAG, "doPlayWebUrlAVTransport for device: " + device + " and URI " + uri);

		getTransportInfo(requestId, service, device, new ICallback() {
			@Override
			public void execute(Object... args) {
				// Handle stopped state
				if (args[0] != null
						&& (args[0].toString().equalsIgnoreCase("STOPPED") || args[0].toString().equalsIgnoreCase(
								"NO_MEDIA_PRESENT"))) {
					setAVTransportURI(requestId, service, device, uri, meta, new ICallback() {
						@Override
						public void execute(Object... args) {
							doPlay(requestId, service, true, new ICallback() {
								@Override
								public void execute(Object... args) {
									runtime.sendContextEvent(requestId, new SecuredContextInfo(new AmbientMediaResult(
											true, "Successfully called Play!"), PrivacyRiskLevel.LOW));
								}
							});
						}
					});
				}
				/*
				 * Handle playing state. Note that, one some players, the transport will be locked and setAVTransportURI
				 * will not be possible until stop has been called. As a precaution, we always stop playback first.
				 */
				else if (args[0] != null && args[0].toString().equalsIgnoreCase("PLAYING")) {
					// Stop playback to free up the device
					doStopPlayback(requestId, device, new Runnable() {
						@Override
						public void run() {
							// Set the transport URI
							setAVTransportURI(requestId, service, device, uri, meta, new ICallback() {
								@Override
								public void execute(Object... args) {
									// Call play
									doPlay(requestId, service, true, new ICallback() {
										@Override
										public void execute(Object... args) {
											runtime.sendContextEvent(requestId, new SecuredContextInfo(
													new AmbientMediaResult(true, "Successfully called Play!"),
													PrivacyRiskLevel.LOW));
										}
									});
								}
							});
						}
					});
				} else {
					runtime.sendContextRequestError(requestId, "Unknown state during getTransportInfo: " + args[0],
							ErrorCodes.NOT_SUPPORTED);
				}
			}
		});
	}

	private void
    setAVTransportURI(final UUID requestId, final Service service, final Device device, String uri,
                String meta, final ICallback callback) {
		// Access the 'SetAVTransportURI' action of the transport service
		Action setUriAction = service.getAction("SetAVTransportURI");
		if (setUriAction != null) {
			/*
			 * Note that the LG HW300Y device supports JPEG images in the following max resolutions: 1) Baseline: 15360w
			 * x 8640h; and 2) Progressive: 1920w x 1440h. For real implementations, we should check the supported
			 * resolutions.
			 */
			ActionInvocation setUriInvoke = new ActionInvocation(setUriAction);
			setUriInvoke.setInput("InstanceID", new UnsignedIntegerFourBytes(0));
			setUriInvoke.setInput("CurrentURI", uri);
			setUriInvoke.setInput("CurrentURIMetaData", meta);
			Log.i(TAG, "Calling SetAVTransportURI");
			ActionCallback setUriActionCallback = new ActionCallback(setUriInvoke) {
				@Override
				public void success(ActionInvocation arg0) {
					Log.i(TAG, "Successfully called SetAVTransportURI");
					if (callback != null)
						callback.execute();
				}

				@Override
				public void failure(ActionInvocation arg0, UpnpResponse arg1, String message) {
					Log.i(TAG, "Failed to call SetAVTransportURI: " + message);
					runtime.sendContextRequestError(requestId, "Failed to call SetAVTransportURI: " + message,
							ErrorCodes.NOT_SUPPORTED);
				}
			};
			// Execute the command using the UPnPService, by providing the callback
			upnpService.getControlPoint().execute(setUriActionCallback);
		} else {
			runtime.sendContextRequestError(requestId, "No SetAVTransportURI Action", ErrorCodes.NOT_SUPPORTED);
		}
	}

	private void getTransportInfo(final UUID requestId, final Service service, final Device device,
			final ICallback callback) {
		// First, find the transport status (e.g., playing or stopped)
		Action statusAction = service.getAction("GetTransportInfo");
		if (statusAction != null) {
			ActionInvocation statusInvoke = new ActionInvocation(statusAction);
			statusInvoke.setInput("InstanceID", new UnsignedIntegerFourBytes(0));
			Log.i(TAG, "Calling GetTransportInfo");
			ActionCallback getStatusCallback = new ActionCallback(statusInvoke) {
				@Override
				public void success(ActionInvocation invocation) {
					Log.i(TAG, "Successfully called GetTransportInfo");
					ActionArgumentValue state = invocation.getOutput("CurrentTransportState");
					Log.i(TAG, "Got transport state: " + state.getValue());
					if (callback != null)
						callback.execute(state);
				}

				@Override
				public void failure(ActionInvocation invocation, UpnpResponse operation, String defaultMsg) {
					runtime.sendContextRequestError(requestId, "Could not access transport state: " + defaultMsg,
							ErrorCodes.NOT_SUPPORTED);
				}
			};
			// Execute the command using the UPnPService, by providing the callback
			upnpService.getControlPoint().execute(getStatusCallback);
		} else {
			runtime.sendContextRequestError(requestId,
					"Could not access GetTransportInfo action for device: " + device.getDisplayString(),
					ErrorCodes.NOT_SUPPORTED);
		}
	}

	private void doPlay(final UUID requestId, final Service service, boolean startTransport, final ICallback callback) {
		Action playAction;
		if (startTransport)
			playAction = service.getAction("Play");
		else
			playAction = service.getAction("Next");
		if (playAction != null) {
			Log.i(TAG, "Playing with startTransport: " + startTransport);
			ActionInvocation playInvoke = new ActionInvocation(playAction);
			playInvoke.setInput("InstanceID", new UnsignedIntegerFourBytes(0));
			if (startTransport)
				playInvoke.setInput("Speed", "1"); // Note: '1' is a string!
			Log.i(TAG, "Calling Play or Next");
			ActionCallback playCallback = new ActionCallback(playInvoke) {
				@Override
				public void success(ActionInvocation invocation) {
					Log.i(TAG, "Successfully Calling Play or Next");
					if (callback != null)
						callback.execute();
				}

				@Override
				public void failure(ActionInvocation invocation, UpnpResponse operation, String defaultMsg) {
					runtime.sendContextRequestError(requestId, "Play failed with error: " + defaultMsg,
							ErrorCodes.NOT_SUPPORTED);
				}
			};
			// Execute the command using the UPnPService, by providing the callback
			upnpService.getControlPoint().execute(playCallback);
		} else {
			runtime.sendContextRequestError(requestId, "Device missing Play action", ErrorCodes.CONFIGURATION_ERROR);
		}
	}

	private void doStopPlayback(final UUID requestId, final Device device, final Runnable finalizer) {
		Log.i(TAG, "doStopPlayback for device: " + device);
		// Make sure the device has an AVTransport service
		final Service transportService = device.findService(new UDAServiceId("AVTransport"));
		if (transportService != null) {
			Action stopAction = transportService.getAction("Stop");
			if (stopAction != null) {
				Log.i(TAG, "Found Stop action");
				ActionInvocation stopInvoke = new ActionInvocation(stopAction);
				stopInvoke.setInput("InstanceID", new UnsignedIntegerFourBytes(0));
				Log.i(TAG, "Calling Stop!" + transportService);
				ActionCallback stopCallback = new ActionCallback(stopInvoke) {
					@Override
					public void success(ActionInvocation invocation) {
						Log.i(TAG, "Successfully called Stop!" + transportService);
						if (finalizer != null) {
							finalizer.run();
						}
					}

					@Override
					public void failure(ActionInvocation invocation, UpnpResponse operation, String defaultMsg) {
						runtime.sendContextRequestError(requestId, "Stop failed with error: " + defaultMsg,
								ErrorCodes.NOT_SUPPORTED);
					}
				};
				// Execute the command using the UPnPService, by providing the callback
				upnpService.getControlPoint().execute(stopCallback);
			} else
				runtime.sendContextRequestError(requestId, "No Stop action for device: " + device.getDisplayString(),
						ErrorCodes.NOT_SUPPORTED);
		} else
			runtime.sendContextRequestError(requestId, "No AVTranport Service for device:  " + device.getDisplayString(),
					ErrorCodes.NOT_SUPPORTED);
	}
}
