package org.ambientdynamix.contextplugins.ambientmedia.airplay;

import android.os.AsyncTask;
import android.util.Log;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by workshop on 12/5/13.
 */
public class HTTPRequestMaker{

    private final String TAG = this.getClass().getSimpleName();
    protected String password;
    private String hostname;
    private int port = -1;
    protected Auth auth;
    private String name;
    protected String authorization;
    private String username;
    private Map params;


    public HTTPRequestMaker(String hostname, int port) {
        this.hostname = hostname;
        this.port = port;
    }
    public HTTPRequestMaker(String hostname) {
        this.hostname = hostname;
    }

    private HTTPRequestMaker.Callback<String> doNothing = new HTTPRequestMaker.Callback<String>() {
        @Override
        public void execute(String result) {
            return;
        }
    };
    public void doHTTP(String method, String uri, final HTTPRequestMaker.Callback callback) throws IOException {
        doHTTP(method, uri, null,callback==null ? doNothing : callback);
    }

    public void doHTTP(String method, String uri, ByteArrayOutputStream os, final HTTPRequestMaker.Callback callback) throws IOException {
        doHTTP(method, uri, os, null,callback==null ? doNothing : callback);
    }

    public void doHTTP(String method, String uri, ByteArrayOutputStream os, Map headers, final HTTPRequestMaker.Callback callback) throws IOException {
        doHTTP(method, uri, os, headers, true, callback == null ? doNothing : callback);
    }

    public void doHTTP(final String method, final String uri, final ByteArrayOutputStream os,  Map headerMap, final boolean repeat, final Callback callback) throws IOException {
        final Map headers;
        if(headerMap == null)
            headers = new HashMap();
        else
            headers = headerMap;
        new AsyncTask<Void, Void, String>() {

            @Override
            protected String doInBackground(Void... voids) {
                URL url = null;
                try {
                    url = new URL("http://" + hostname + (port != -1 ? ":" + port : "") + uri);
                    Log.i(TAG, "sending request to "+  url.toString());
                } catch (MalformedURLException e) {
                    return "";
                }
                try {
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setUseCaches(false);
                    conn.setDoOutput(true);
                    conn.setRequestMethod(method);

                    if (params != null) {
                        //Try to reuse password if already set
                        headers.put("Authorization", makeAuthorization(params, password, method, uri));
                    }
                    if (headers.size() > 0) {
                        conn.setRequestProperty("User-Agent", "MediaControl/1.0");
                        Object[] keys = headers.keySet().toArray();
                        for (int i = 0; i < keys.length; i++) {
                            conn.setRequestProperty((String) keys[i], (String) headers.get(keys[i]));
                        }
                    }

                    if (os != null) {
                        byte[] data = os.toByteArray();
                        conn.setRequestProperty("Content-Length", "" + data.length);
                    }
                    try{
                        conn.connect();
                    }   catch(Exception e){
                        return "";
                    }
                    if (os != null) {
                        os.writeTo(conn.getOutputStream());
                        os.flush();
                        os.close();
                    }

                    if (conn.getResponseCode() == 401) {
                        if (repeat) {
                            String authstring = conn.getHeaderFields().get("WWW-Authenticate").get(0);
                            if (setPassword() != null) {
                                params = getAuthParams(authstring);
                                doHTTP(method, uri, os, headers, false, callback);
                                return null;
                            } else {
                                return null;
                            }
                        } else {
                            throw new IOException("Incorrect password");
                        }
                    } else {
                        String result;
                            InputStream is = conn.getInputStream();
//                            Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
//                            result = s.hasNext() ? s.next() : "";
                        BufferedReader br = null;
                        StringBuilder sb = new StringBuilder();

                        String line;
                        try {

                            br = new BufferedReader(new InputStreamReader(is));
                            while ((line = br.readLine()) != null) {
                                sb.append(line);
                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                        } finally {
                            if (br != null) {
                                try {
                                    br.close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        result = sb.toString();
                        if(result.equals(""))
                            return Integer.toString(conn.getResponseCode()) + " " + conn.getResponseMessage();
                        return result;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }


            @Override
            protected void onPostExecute(String result) {
                if(result!=null && callback != null)
                    callback.execute(result);
            }
        }.execute();
    }

    protected String setPassword() throws IOException {
        if (password != null) {
            return password;
        } else {
            if (auth != null) {
                password = auth.getPassword(hostname, name);
                return password;
            } else {
                throw new IOException("Authorisation requied");
            }
        }
    }

    //Auth classes
    public static interface Auth {
        public abstract String getPassword(String hostname, String name);
    }

    public interface Callback<T> {
        public void execute(T result);
    }

    public static class AuthDialog implements Auth {
        private Window parent;

        public AuthDialog(Window parent) {
            this.parent = parent;
        }

        public String getPassword(String hostname, String name) {
            final JPasswordField password = new JPasswordField();
            JOptionPane optionPane = new JOptionPane(password, JOptionPane.PLAIN_MESSAGE, JOptionPane.OK_CANCEL_OPTION);
            JDialog dialog = optionPane.createDialog(parent, "Password:");
            dialog.setLocationRelativeTo(parent);
            dialog.setVisible(true);
            int result = (Integer) optionPane.getValue();
            dialog.dispose();
            if (result == JOptionPane.OK_OPTION) {
                return new String(password.getPassword());
            }
            return null;
        }
    }
    public void setAuth(Auth auth) {
        this.auth = auth;
    }

    protected Map getAuthParams(String authString) {
        Map params = new HashMap();
        int firstSpace = authString.indexOf(' ');
        String digest = authString.substring(0, firstSpace);
        String rest = authString.substring(firstSpace + 1).replaceAll("\r\n", " ");
        String[] lines = rest.split("\", ");
        for (int i = 0; i < lines.length; i++) {
            int split = lines[i].indexOf("=\"");
            String key = lines[i].substring(0, split);
            String value = lines[i].substring(split + 2);
            if (value.charAt(value.length() - 1) == '"') {
                value = value.substring(0, value.length() - 1);
            }
            params.put(key, value);
        }
        return params;
    }

    protected String makeAuthorization(Map params, String password, String method, String uri) {
        String realm = (String) params.get("realm");
        String nonce = (String) params.get("nonce");
        String ha1 = md5Digest(username + ":" + realm + ":" + password);
        String ha2 = md5Digest(method + ":" + uri);
        String response = md5Digest(ha1 + ":" + nonce + ":" + ha2);
        authorization = "Digest username=\"" + username + "\", "
                + "realm=\"" + realm + "\", "
                + "nonce=\"" + nonce + "\", "
                + "uri=\"" + uri + "\", "
                + "response=\"" + response + "\"";
        return authorization;
    }

    protected String md5Digest(String input) {
        byte[] source;
        try {
            //Get byte according by specified coding.
            source = input.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            source = input.getBytes();
        }
        String result = null;
        char hexDigits[] = {'0', '1', '2', '3', '4', '5', '6', '7',
                '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(source);
            //The result should be one 128 integer
            byte temp[] = md.digest();
            char str[] = new char[16 * 2];
            int k = 0;
            for (int i = 0; i < 16; i++) {
                byte byte0 = temp[i];
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];
                str[k++] = hexDigits[byte0 & 0xf];
            }
            result = new String(str);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;

    }
}
