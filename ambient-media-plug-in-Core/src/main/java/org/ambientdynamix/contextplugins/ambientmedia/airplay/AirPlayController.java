package org.ambientdynamix.contextplugins.ambientmedia.airplay;

import java.io.IOException;
import java.net.InetAddress;

import javax.jmdns.*;

import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.MulticastLock;
import android.text.format.Formatter;
import android.util.Log;
import org.ambientdynamix.contextplugins.ambientmedia.AmbientMediaDevice;
import org.ambientdynamix.contextplugins.ambientmedia.BaseMediaController;

public class AirPlayController extends BaseMediaController implements ServiceListener, ServiceTypeListener {
	private final String TAG = this.getClass().getSimpleName();
	private WifiManager wifiManager;
	private MulticastLock mCastLock;
	private JmDNS jmdns;
	private ServiceListener listener = null;
	private final static String AIRPLAY_TYPE = "_airplay._tcp.local.";
	private final static String AIRTUNES_TYPE = "_raop._tcp.local.";

	public final static String TYPE_TOUCH_ABLE_TYPE = "_touch-able._tcp.local.";
	public final static String TYPE_DACP_TYPE = "_dacp._tcp.local.";
	public final static String TYPE_REMOTE_TYPE = "_touch-remote._tcp.local.";
	public final static String HOSTNAME = "AmbientMediaPlugin";
    public static final String DNSSD_TYPE = "_airplay._tcp.local.";

	public AirPlayController(WifiManager wifiManager) {
		this.wifiManager = wifiManager;
	}

	@Override
	public void init() throws Exception {
		// Init MD5
		initMD5();
	}

	@Override
	public void start() {
		try {
			/*
			 * Make sure to use a later version of JMDMS to ensure that discovery and resolving work properly:
			 * http://download.ecf-project.org/maven/javax/jmdns/jmdns/3.4.2-SNAPSHOT/
			 */
			String ip = Formatter.formatIpAddress(wifiManager.getConnectionInfo().getIpAddress());
			Log.i(TAG, "Initializing JmDNS at " + ip);
			// start multicast lock
			mCastLock = wifiManager.createMulticastLock("Ambient Media Lock");
			mCastLock.setReferenceCounted(true);
			mCastLock.acquire();
			jmdns = JmDNS.create(InetAddress.getByName(ip), HOSTNAME);
			Log.i(TAG, "Created jmdns: " + jmdns);
			jmdns.addServiceListener(AIRPLAY_TYPE, this);
//			jmdns.addServiceListener(AIRTUNES_TYPE, this); //Airtunes support is not working yet
            jmdns.addServiceTypeListener(this);
			// jmdns.addServiceListener(TYPE_DACP_TYPE, this);
			// jmdns.addServiceListener(TYPE_REMOTE_TYPE, this);
			// jmdns.addServiceListener(TYPE_TOUCH_ABLE_TYPE, this);
		} catch (IOException e) {
			Log.w(TAG, "Could not create jmdns " + e);
		}
	}

	@Override
	public void stop() {
		if (jmdns != null) {
			// Shutdown the AirPlay service
			if (listener != null) {
				jmdns.removeServiceListener(AIRPLAY_TYPE, listener);
				listener = null;
			}
			try {
				jmdns.unregisterAllServices();
			} catch (Exception e1) {
				Log.w(TAG, "Problem unregisterAllServices: " + e1.toString());
			}
			try {
				jmdns.close();
			} catch (IOException e) {
				Log.w(TAG, "Problem closing JMDNS: " + e.toString());
			}
			jmdns = null;
			if (mCastLock != null) {
				mCastLock.release();
				mCastLock = null;
			}
			jmdns = null;
		}
	}

	@Override
	public void close() {
		stop();
	}

	@Override
	public void serviceResolved(ServiceEvent ev) {
		Log.i(TAG, "Zeroconf serviceResolved: " + ev.getInfo().getQualifiedName());
		Log.i(TAG, "Zeroconf IP: " + ev.getInfo().getInetAddresses()[0]);
		Log.i(TAG, "Zeroconf IP count: " + ev.getInfo().getInetAddresses().length);
		// Ensure an IP address
		if (ev.getInfo().getInetAddresses() != null && ev.getInfo().getInetAddresses().length > 0) {
			// Create device
			StringBuilder name = new StringBuilder();
            AmbientMediaDevice d = null;
			if(ev.getInfo().getTypeWithSubtype().contains("_airplay")){
				name.append("[AirPlay] " + ev.getInfo().getName());
                try {
                    d = new AirPlayDevice(ev.getInfo(), createId(ev.getInfo()), name.toString(),
                            "MediaRenderer");
                    d.setup();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }else if(ev.getInfo().getTypeWithSubtype().contains("_raop")){
                name.append("[AirTunes] " + ev.getInfo().getName());
                d = new AirTunesDevice(ev.getInfo(),createId(ev.getInfo()),name.toString(),"MediaRenderer",ev.getInfo().getInetAddresses()[0],ev.getInfo().getPort());
            }
            if(d != null){
                notifyDeviceDiscovered(d);
                Log.i(TAG, "Zeroconf calling notifyDeviceDiscovered for: " + d.getDisplayName());
            }
		}
	}

	@Override
	public void serviceRemoved(ServiceEvent ev) {
		Log.i(TAG, "Zeroconf serviceRemoved: " + ev.getInfo().getQualifiedName());
		notifyDeviceLost(createId(ev.getInfo()));
	}

	@Override
	public void serviceAdded(ServiceEvent event) {
		Log.i(TAG, "Zeroconf serviceAdded: " + event.getInfo().getQualifiedName());
		// Required to force serviceResolved to be called again (after the first search)
        if(event.getInfo().getQualifiedName().contains("_airplay"))
            jmdns.requestServiceInfo(event.getType(), event.getName(), 1);
	}

	@Override
	public void searchForDevices() {
		if (jmdns != null) {
            ServiceInfo[] infos = jmdns.list(DNSSD_TYPE, 3000);
            for (int i = 0; i < infos.length; i++) {
                ServiceInfo info = infos[i];
                jmdns.requestServiceInfo(info.getType(), info.getName(), 1);
            }
        }
	}



	private String createId(ServiceInfo si) {
		StringBuilder id = new StringBuilder();
		id.append(si.getQualifiedName());
		id.append(si.getInetAddresses()[0].getHostAddress());
		id.append(si.getPort());
		return md5(id.toString());
	}

    public void streamAudio(){

    }

    @Override
    public void serviceTypeAdded(ServiceEvent serviceEvent) {
        Log.i(TAG, "Zeroconf serviceAdded: " + serviceEvent.getInfo().getQualifiedName());
        Log.i(TAG, "Zeroconf IP: " + serviceEvent.getInfo().getInetAddresses()[0]);
        Log.i(TAG, "Zeroconf IP count: " + serviceEvent.getInfo().getInetAddresses().length);
    }

    @Override
    public void subTypeForServiceTypeAdded(ServiceEvent serviceEvent) {
        Log.i(TAG, "Zeroconf serviceAdded: " + serviceEvent.getInfo().getQualifiedName());
        Log.i(TAG, "Zeroconf IP: " + serviceEvent.getInfo().getInetAddresses()[0]);
        Log.i(TAG, "Zeroconf IP count: " + serviceEvent.getInfo().getInetAddresses().length);
    }
}
