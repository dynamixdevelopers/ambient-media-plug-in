package org.ambientdynamix.contextplugins.ambientmedia.airplay;

import android.os.AsyncTask;
import android.util.Log;
import com.dd.plist.NSDictionary;
import com.dd.plist.PropertyListParser;
import org.ambientdynamix.api.contextplugin.security.PrivacyRiskLevel;
import org.ambientdynamix.contextplugins.ambientcontrol.Commands;
import org.ambientdynamix.contextplugins.ambientcontrol.DisplayCommand;
import org.ambientdynamix.contextplugins.ambientmedia.AmbientMediaDevice;
import org.ambientdynamix.contextplugins.ambientmedia.AmbientMediaPlaybackInfo;
import org.ambientdynamix.contextplugins.ambientmedia.util.YoutubeUrlParser;
import org.ambientdynamix.contextplugins.ambientmedia.util.NsObjectParser;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import javax.jmdns.ServiceInfo;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.*;
import java.util.List;

public class AirPlayDevice extends AmbientMediaDevice {
    private final String TAG = this.getClass().getSimpleName();
    public static final String DNSSD_TYPE = "_airplay._tcp.local.";

    public static final String NONE = "None";
    public static final String SLIDE_LEFT = "SlideLeft";
    public static final String SLIDE_RIGHT = "SlideRight";
    public static final String DISSOLVE = "Dissolve";
    public static final String USERNAME = "Airplay";
    public static final int PORT = 7000;
    public static final int APPLETV_WIDTH = 1280;
    public static final int APPLETV_HEIGHT = 720;
    public static final float APPLETV_ASPECT = (float) APPLETV_WIDTH / APPLETV_HEIGHT;

    protected String hostname;
    protected String name;
    protected int port;


    protected Map params;
    protected HTTPRequestMaker.Auth auth;
    protected int appletv_width = APPLETV_WIDTH;
    protected int appletv_height = APPLETV_HEIGHT;
    protected float appletv_aspect = APPLETV_ASPECT;
    private ServiceInfo serviceInfo;
    private String password;
    private boolean splash = true;
    private boolean stop;

    public AirPlayDevice(ServiceInfo serviceInfo, String id, String displayName, String type) throws IOException {
        super(id, displayName, type);
        this.serviceInfo = serviceInfo;
        for (String addresses : serviceInfo.getHostAddresses()) {
            if(addresses.matches(IPV4_REGEX))
                this.hostname = addresses;
        }
        if(hostname==null)
            throw new IOException("no valid IP for hostname");
        this.port = serviceInfo.getPort();
        this.name = serviceInfo.getName();
    }

    private static final String IPV4_REGEX = "^(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})$";

    @Override
    public void setup() {
        Log.i(TAG, "setup");
        try {
//            new HTTPRequestMaker(hostname, port).doHTTP("GET", "/server-info", new HTTPRequestMaker.Callback<String>() {
//                @Override
//                public void execute(String result) {
//                    Log.i(TAG, result);
//                    try {
//                        NSDictionary rootDict = (NSDictionary) PropertyListParser.parse(new ByteArrayInputStream(result.getBytes("UTF-8")));
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//            });
            displayPicture("http://repo.ambientdynamix.org/dynamix/logos/dynamix_logo_white_on_black_large.png", DISSOLVE);
//            runtime.getControlConnectionManager().sendControllCommand(new DisplayCommand(CommandsEnum.DISPLAY_COLOR,150,150,150), CommandsEnum.DISPLAY_COLOR);
        } catch (IOException e) {
            Log.e(TAG, "server unreachable");
        } catch(Exception e){
            Log.e(TAG, "error during device setup: " + e);
            e.printStackTrace();
        }


    }

    @Override
    public void play(UUID requestId, String url, String title) {
        splash = false;
        stop = false;
        playStateTracker = new PlayStateTracker();
        playStateTracker.execute(requestId);
        Log.i(TAG, "play " + url);
        try {

            if (url.contains("youtube"))
                playYoutube(url, requestId);
            else
                displayPicture(url, DISSOLVE);
            runtime.getControlConnectionManager().sendControllCommand(new DisplayCommand(Commands.DISPLAY_COLOR, 0, 255, 0,"Playback Color"));

        } catch (IOException e) {
            Log.i(TAG, "server unreachable beacause of: " + e);
        }

    }

    private void displayPicture(String urlString, String transistion) throws IOException {
        splash = false;
        if (urlString.startsWith("http")) {
            InputStream is = new URL(urlString).openStream();
            ByteArrayOutputStream image = new ByteArrayOutputStream();
            byte buf[] = new byte[1024];
            int len;
            while ((len = is.read(buf)) > 0) {
                image.write(buf, 0, len);
            }
            image.close();
            photo(image, transistion);
        } else
            photo(urlString);


    }


    private void playYoutube(String url, final UUID requestId) throws UnsupportedEncodingException, MalformedURLException {

        YoutubeUrlParser youtubeUrl = new YoutubeUrlParser(url);
        youtubeUrl.getQueryString(new YoutubeUrlParser.CallbackOnURLReady() {
            @Override
            public void callback(Map<String, String> atvSupportedUrls) {
                try {
                    List<NameValuePair> params = new ArrayList<NameValuePair>();
//            params.add(new BasicNameValuePair("Content-Location", "https://archive.org/download/MainFram1984/MainFram1984_64kb.mp4"));

                    for (String s : atvSupportedUrls.keySet()) {
                        Log.i(TAG, "available qualities: " + s);
                    }
                    String contentLocation = atvSupportedUrls.containsValue("22") ?
                            atvSupportedUrls.get("22") : (
                            atvSupportedUrls.containsValue("18") ? atvSupportedUrls.get("18") : null);
                    if (contentLocation == null && atvSupportedUrls.size() > 0)
                        contentLocation = atvSupportedUrls.get(atvSupportedUrls.keySet().iterator().next());
                    params.add(new BasicNameValuePair("Content-Location", contentLocation));
                    params.add(new BasicNameValuePair("Start-Position", "0"));
                    Log.i(TAG, "requesting to play video URL: " + contentLocation);

                    ByteArrayOutputStream os = new ByteArrayOutputStream();
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                    writer.write(getRequestParams(params));
                    writer.flush();
                    writer.close();
                    Map<String, String> headers = new HashMap<String, String>();

                    headers.put("Content-Type", "text/parameters");
                    new HTTPRequestMaker(hostname, port).doHTTP("POST", "/play", os, new HTTPRequestMaker.Callback<String>() {
                        @Override
                        public void execute(String result) {
                            Log.i(TAG, result);
                        }
                    });


                } catch (Exception e) {
                    Log.i(TAG, e.toString());
                }
            }
        });
    }

    @Override
    public void pause(final UUID requestId) {
        try {
            if (!playStateTracker.isReadyToPlay()){
                stop = false;
                play(null,"http://www.youtube.com/watch?v=lX6JcybgDFo","demo");
            }
            if (playStateTracker.getCurrentRate() != 1) {
                new HTTPRequestMaker(hostname, port).doHTTP("POST", "/rate?value=1", new HTTPRequestMaker.Callback<String>() {
                    @Override
                    public void execute(String result) {
                        runtime.getControlConnectionManager().sendControllCommand(new DisplayCommand(Commands.DISPLAY_COLOR, 0, 255, 0,"Playback Color"));
                        Log.i(TAG, result);
                    }
                });
            } else {
                new HTTPRequestMaker(hostname, port).doHTTP("POST", "/rate?value=0.000000", new HTTPRequestMaker.Callback<String>() {
                    @Override
                    public void execute(String result) {
                        runtime.getControlConnectionManager().sendControllCommand(new DisplayCommand(Commands.DISPLAY_COLOR, 0, 255, 255,"Playback Color"));
                        Log.i(TAG, result);
                    }
                });
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void teardown(UUID requestId) {
        splash = false;
        Log.i(TAG, "teardown");
        try {
            new HTTPRequestMaker(hostname, port).doHTTP("POST", "/stop", new HTTPRequestMaker.Callback<String>() {
                @Override
                public void execute(String result) {
                    runtime.getControlConnectionManager().sendControllCommand(new DisplayCommand(Commands.DISPLAY_COLOR, 0, 0, 0,"Playback Color"));
                    Log.i(TAG, result);
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void forwardSeek(final UUID requestId) {
        try {
            if (!playStateTracker.isReadyToPlay())
                return;
            if (playStateTracker.getCurrentRate() <= 1) {
                new HTTPRequestMaker(hostname, port).doHTTP("POST", "/rate?value=3.0", new HTTPRequestMaker.Callback<String>() {
                    @Override
                    public void execute(String result) {
                        runtime.getControlConnectionManager().sendControllCommand(new DisplayCommand(Commands.DISPLAY_COLOR, 0, 0, 255,"Playback Color"));
                        Log.i(TAG, result);
                    }
                });
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void backwardSeek(final UUID requestId) {
        try {
            if (!playStateTracker.isReadyToPlay())
                return;
            if (playStateTracker.getCurrentRate() >= 0) {
                new HTTPRequestMaker(hostname, port).doHTTP("POST", "/rate?value=-3.0", new HTTPRequestMaker.Callback<String>() {
                    @Override
                    public void execute(String result) {
                        runtime.getControlConnectionManager().sendControllCommand(new DisplayCommand(Commands.DISPLAY_COLOR, 255, 0, 255,"Playback Color"));
                        Log.i(TAG, result);
                    }
                });
            }


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void stop(UUID requestId) {
        try {
            if (stop)
                return;
            stop = true;
            splash = true;
            new HTTPRequestMaker(hostname, port).doHTTP("POST", "/stop", new HTTPRequestMaker.Callback<String>() {
                @Override
                public void execute(String result) {
                    try {
                        displayPicture("http://repo.ambientdynamix.org/dynamix/logos/dynamix_logo_white_on_black_large.png", DISSOLVE);
                        runtime.getControlConnectionManager().sendControllCommand(new DisplayCommand(Commands.DISPLAY_COLOR, 150, 150, 150,"Playback Color"));

                        new splashScreen().execute();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Log.i(TAG, result);
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public class splashScreen extends AsyncTask<UUID, Void, Void> {

        @Override
        protected Void doInBackground(UUID... uuids) {
            try {
                if (splash) {
                    displayPicture("http://repo.ambientdynamix.org/dynamix/logos/dynamix_logo_white_on_black_large.png", DISSOLVE);
                    Thread.sleep(10000);
                    new splashScreen().execute();
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public class PlayStateTracker extends AsyncTask<UUID, Void, Void> {
        private HTTPRequestMaker httpRequestMaker = new HTTPRequestMaker(hostname, port);

        private boolean readyToPlay;
        private double currentRate;
        private double currentPosition;


        private HTTPRequestMaker.Callback<String> playingCallback = new HTTPRequestMaker.Callback<String>() {
            @Override
            public void execute(String result) {
                try {
                    NsObjectParser parser = new NsObjectParser(result);
                    readyToPlay = parser.getBoolean("readyToPlay");
                    currentPosition = parser.getDouble("position");
                    currentRate = parser.getDouble("rate");
                    runtime.sendBroadcastContextEvent(new AmbientMediaPlaybackInfo(currentPosition, currentRate), PrivacyRiskLevel.LOW);
//                    Log.i(TAG, "video playback state: position(" + currentPosition + ") at rate(" + currentRate + ")");
                    if (readyToPlay) {
                        Thread.sleep(500);
                        httpRequestMaker.doHTTP("GET", "/playback-info", playingCallback);
                    } else if(stop) {
                        count = 0;
                        httpRequestMaker.doHTTP("GET", "/playback-info", waitToPlayCallback);
                    }

                } catch (Exception e) {
                    Log.e(TAG, "e");
                }
            }

            ;
        };
        int count = 0;
        private HTTPRequestMaker.Callback<String> waitToPlayCallback = new HTTPRequestMaker.Callback<String>() {

            @Override
            public void execute(String result) {
                try {
//                    Log.i(TAG, "video playback state: ready to play: " + readyToPlay + " position(" + currentPosition + ") at rate(" + currentRate + ")");
                    NsObjectParser parser = new NsObjectParser(result);
                    readyToPlay = parser.getBoolean("readyToPlay");
                    if (!readyToPlay) {
                        count++;
                        if (count < 5) {
                            Thread.sleep(500);
                            httpRequestMaker.doHTTP("GET", "/playback-info", waitToPlayCallback);
                        }
                    } else
                        httpRequestMaker.doHTTP("GET", "/playback-info", playingCallback);

                } catch (Exception e) {
                    Log.e(TAG, "e");
                }
            }

            ;
        };


        @Override
        protected Void doInBackground(UUID... uuids) {
            try {
                httpRequestMaker.doHTTP("GET", "/playback-info", waitToPlayCallback);
            } catch (Exception e) {
                Log.e(TAG, "e");
            }
            return null;
        }

        public double getCurrentRate() {
            return currentRate;
        }

        public double getCurrentPosition() {
            return currentPosition;
        }

        public boolean isReadyToPlay() {
            return readyToPlay;
        }
    }

    ;

    private PlayStateTracker playStateTracker;

    //AirPlay class

    public void setScreenSize(int width, int height) {
        appletv_width = width;
        appletv_height = height;
        appletv_aspect = (float) width / height;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setAuth(HTTPRequestMaker.Auth auth) {
        this.auth = auth;
    }

    public void stop() {
        try {
            new HTTPRequestMaker(hostname, port).doHTTP("POST", "/stop", null);
            params = null;
        } catch (Exception e) {
        }
    }

//    protected BufferedImage scaleImage(BufferedImage image) {
//        int width = image.getWidth();
//        int height = image.getHeight();
//        if (width <= appletv_width && height <= appletv_height) {
//            return image;
//        } else {
//            int scaledheight;
//            int scaledwidth;
//            float image_aspect = (float) width / height;
//            if (image_aspect > appletv_aspect) {
//                scaledheight = new Float(appletv_width / image_aspect).intValue();
//                scaledwidth = appletv_width;
//            } else {
//                scaledheight = appletv_height;
//                scaledwidth = new Float(appletv_height * image_aspect).intValue();
//            }
//            BufferedImage scaledimage = new BufferedImage(scaledwidth, scaledheight, BufferedImage.TYPE_INT_RGB);
//            Graphics2D g = scaledimage.createGraphics();
//            g.drawImage(image, 0, 0, scaledwidth, scaledheight, null);
//            g.dispose();
//            return scaledimage;
//        }
//    }

    public void photo(String filename) throws IOException {
        this.photo(filename, NONE);
    }

    public void photo(String filename, String transition) throws IOException {
        this.photo(new File(filename), transition);
    }


    public void photo(File imagefile, String transition) throws IOException {
        BufferedInputStream buf = new BufferedInputStream(new FileInputStream(imagefile));
        photo(buf, transition, (int) imagefile.length());
    }


    public void photo(BufferedInputStream buf, String transition, int size) throws IOException {
        byte[] bytes = new byte[size];
        buf.read(bytes, 0, size);
        buf.close();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        baos.write(bytes);
        photo(baos, transition);
    }

    public void photo(File image) throws IOException {
        this.photo(image, NONE);
    }

    public void photo(ByteArrayOutputStream image, String transition) throws IOException {
        photoRaw(image, transition);
//        photothread = new PhotoThread(this, image, 5000);
//        photothread.start();
    }

    protected void photoRaw(ByteArrayOutputStream image, String transition) throws IOException {
        Map headers = new HashMap();
        headers.put("X-Apple-Transition", transition);
        new HTTPRequestMaker(hostname, port).doHTTP("PUT", "/photo", image, headers, null);
    }

    public class PhotoThread extends Thread {
        private final AirPlayDevice airplay;
        private ByteArrayOutputStream image = null;
        private int timeout = 5000;

        public PhotoThread(AirPlayDevice airplay) {
            this(airplay, null, 1000);
        }

        public PhotoThread(AirPlayDevice airplay, ByteArrayOutputStream image, int timeout) {
            this.airplay = airplay;
            this.image = image;
            this.timeout = timeout;
        }

        public void run() {
            while (!Thread.interrupted()) {
                try {
                    if (image == null) {
//                        airplay.photoRawCompress(AirPlayDevice.captureScreen(),NONE);
                    } else {
                        airplay.photoRaw(image, NONE);
                        Thread.sleep(Math.round(0.9 * timeout));
                    }
                } catch (InterruptedException e) {
                    break;
                } catch (Exception e) {
                    e.printStackTrace();
                    break;
                }
            }
        }
    }


//    @Override
//        protected String doInBackground(String... params) {
//            if(params.length==0)
//                return;
//            URL url = new URl();
//            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//            conn.setReadTimeout(10000);
//            conn.setConnectTimeout(15000);
//            conn.setRequestMethod("POST");
//            conn.setDoInput(true);
//            conn.setDoOutput(true);
//
//            List<NameValuePair> params = new ArrayList<NameValuePair>();
//            params.add(new BasicNameValuePair("firstParam", paramValue1));
//            params.add(new BasicNameValuePair("secondParam", paramValue2));
//            params.add(new BasicNameValuePair("thirdParam", paramValue3));
//
//            OutputStream os = conn.getOutputStream();
//            BufferedWriter writer = new BufferedWriter(
//                    new OutputStreamWriter(os, "UTF-8"));
//            writer.write(getQuery(params));
//            writer.flush()
//            writer.close();
//            os.close();
//
//            conn.connect();
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//            super.onPostExecute(result);
//            //Do anything with response..
//        }
//    }

    private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params) {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    private String getRequestParams(List<NameValuePair> params) {
        StringBuilder result = new StringBuilder();
        for (NameValuePair param : params) {
            result.append(param.getName()).append(": ").append(param.getValue()).append("\n");
        }
        result.append("\n");
        return result.toString();
    }

}
