/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.ambientmedia;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

/**
 * Base class for media controllers.
 * @author Darren Carlson
 *
 */
public abstract class BaseMediaController implements IMediaController {
	private List<IMediaControllerListener> listeners = new ArrayList<IMediaControllerListener>();
	private MessageDigest digest;

	public void initMD5() throws NoSuchAlgorithmException {
		digest = java.security.MessageDigest.getInstance("MD5");
	}

	/**
	 * Returns a MD5 hash of the incoming string.
	 */
	public String md5(String s) {
		digest.reset();
		digest.update(s.getBytes());
		byte messageDigest[] = digest.digest();
		// Create Hex String
		StringBuffer hexString = new StringBuffer();
		for (int i = 0; i < messageDigest.length; i++)
			hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
		return hexString.toString();
	}

	protected void notifyDeviceDiscovered(AmbientMediaDevice d) {
		synchronized (listeners) {
			for (IMediaControllerListener l : listeners)
				l.onDeviceDetected(d);
		}
	}

	protected void notifyDeviceLost(String id) {
		synchronized (listeners) {
			for (IMediaControllerListener l : listeners)
				l.onDeviceLost(id);
		}
	}

	@Override
	public void addListener(IMediaControllerListener listener) {
		synchronized (listeners) {
			if (listener != null && !listeners.contains(listener))
				listeners.add(listener);
		}
	}

	@Override
	public void removeListener(IMediaControllerListener listener) {
		synchronized (listeners) {
			if (listener != null && listeners.contains(listener))
				listeners.remove(listener);
		}
	}
}
