/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.ambientmedia;

import java.util.*;

import android.os.RemoteException;
import org.ambientdynamix.api.application.ErrorCodes;
import org.ambientdynamix.api.contextplugin.*;
import org.ambientdynamix.api.contextplugin.security.PrivacyRiskLevel;
import org.ambientdynamix.api.contextplugin.security.SecuredContextInfo;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import org.ambientdynamix.contextplugins.ambientcontrol.*;
import org.ambientdynamix.contextplugins.ambientmedia.airplay.AirPlayController;
import org.ambientdynamix.contextplugins.ambientmedia.upnp.UPnPController;

/**
 * Media playback plug-in for AirPlay and UPnP/DLNA devices.
 *
 * @author Darren Carlson
 */
public class AmbientMediaRuntime extends ContextPluginRuntime implements IMediaControllerListener {
    private final String TAG = ((Object) this).getClass().getSimpleName();     //terribly ugly but neccessary because of bug in intelliJ
    private PowerScheme powerScheme;
    private WifiManager wifiManager;
    private ConnectivityManager connectivityManager;
    private Context context;
    private List<IMediaController> mediaControllers = new ArrayList<IMediaController>();
    private ControlConnectionManager controlConnectionManager;

    @Override
    public void setPowerScheme(PowerScheme scheme) {
    }

    @Override
    public void init(PowerScheme scheme, ContextPluginSettings settings) throws Exception {
        this.powerScheme = scheme;
        context = getSecuredContext();
        wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        if (wifiManager == null)
            throw new Exception("Could not access WifiManager - check permissions");
        connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager == null)
            throw new Exception("Could not access ConnectivityManager - check permissions");
        // Setup the media controllers
        mediaControllers.add(new AirPlayController(wifiManager));
        mediaControllers.add(new UPnPController(context, this, wifiManager, connectivityManager));
        // Init each controller
        for (IMediaController c : mediaControllers) {
            c.init();
            c.addListener(this);
        }
        controlConnectionManager = new ControlConnectionManager(this, clientListener, null, new TranslatingProfileMatcher(), TAG);
        Log.d(TAG, "Initialized!");

    }


    private ControlConnectionManager.IControllConectionManagerListener clientListener = new ControlConnectionManager.ControllConnectionManagerListenerAdapter() {

        @Override
        public void consumeCommand(IControlMessage command, String sourcePluginId, UUID request) {
            if (command instanceof ToggleCommand) {
                consumeToggleCommand((ToggleCommand) command, request);
            } else if (command instanceof DisplayCommand) {
                consumeDisplayCommand((DisplayCommand) command, request);

            }
        }

        @Override
        public Map<String, String> provideFeedbackSuggestion() {
            Log.i(TAG, "suggesting feedback:" + Commands.DISPLAY_COLOR.toString());
            HashMap<String, String> commandsEnums = new HashMap<String, String>();
            commandsEnums.put("Playback Color", Commands.DISPLAY_COLOR);
            return commandsEnums;
        }

        private String lastCommand = Commands.MOVEMENT_NEUTRAL;

        public void consumeToggleCommand(ToggleCommand control, UUID uuid) {
            if (lastCommand == control.getCommand())
                return;
            else
                lastCommand = control.getCommand();

            for (AmbientMediaDevice ambientMediaDevice : DomainRouter.getAllDevices()) {
                if (control.getCommand().equals(Commands.PLAYBACK_PLAY_PAUSE)) {
                    ambientMediaDevice.pause(uuid);
                    return;
                } else if (control.getCommand().equals(Commands.PLAYBACK_BACKWARD_SEEK)) {
                    ambientMediaDevice.backwardSeek(uuid);
                    return;
                } else if (control.getCommand().equals(Commands.PLAYBACK_FORWARD_SEEK)) {
                    ambientMediaDevice.forwardSeek(uuid);
                    return;
                } else if (control.getCommand().equals(Commands.PLAYBACK_NEXT)) {
                    return;
                } else if (control.getCommand().equals(Commands.PLAYBACK_PREVIOUS)) {
                    return;
                } else if (control.getCommand().equals(Commands.PLAYBACK_STOP)) {
                    ambientMediaDevice.stop(uuid);
                    return;
                }
            }
        }

        public void consumeDisplayCommand(DisplayCommand command, UUID request) {
            if (command.getCommand().equals(Commands.DISPLAY_VIDEO) || command.getCommand().equals(Commands.DISPLAY_IMAGE)) {
                if (command.getDeviceId() != null) {
                    AmbientMediaDevice device = DomainRouter.getDevice(command.getDeviceId());
                    if (device != null)
                        device.play(request, command.getURL(), command.getMessage());

                } else {
                    for (AmbientMediaDevice ambientMediaDevice : DomainRouter.getAllDevices()) {
                        ambientMediaDevice.play(request, command.getURL(), command.getMessage());
                    }
                }
            }
        }

        @Override
        public void controllRequest(String commandsEnum, String s) {

        }

        @Override
        public void stopControlling(String command, String name) {
            for (AmbientMediaDevice d : DomainRouter.getAllDevices()) {
                d.teardown(null);
            }
        }


    };


//
//    07-02 19:08:47.362: I/AirPlayController(3415): 	---- Services -----
//            07-02 19:08:47.362: I/AirPlayController(3415): 	---- Types ----
//            07-02 19:08:47.362: I/AirPlayController(3415): 	---- cache ----
//            07-02 19:08:47.362: I/AirPlayController(3415): 	---- Service Collectors ----
//            07-02 19:08:47.362: I/AirPlayController(3415): 	---- Service Listeners ----
//            07-02 19:08:47.362: I/AmbientMediaRuntime(3415): onDeviceDetected for: [AirPlay] Apple TV with type MediaRenderer and ID 1a74d245c7ce9c9e14df198efa11f3c
//    07-02 19:08:47.362: I/AmbientMediaRuntime(3415): DomainRouter.addDevice for: [AirPlay] Apple TV with type MediaRenderer
//    07-02 19:08:47.932: I/AirPlayController(3415): Zeroconf serviceAdded: Apple TV._airplay._tcp.local.
//    07-02 19:08:47.962: I/AirPlayController(3415): Zeroconf serviceResolved: Apple TV._airplay._tcp.local.
//    07-02 19:08:47.962: I/AirPlayController(3415): Zeroconf IP: /10.0.1.101
//            07-02 19:08:47.962: I/AirPlayController(3415): Zeroconf IP count: 2
//            07-02 19:08:47.972: I/AirPlayDevice(3415): setup
//    07-02 19:08:48.062: I/AmbientMediaRuntime(3415): onDeviceDetected for: [AirPlay] Apple TV with type MediaRenderer and ID 1a74d245c7ce9c9e14df198efa11f3c
//    07-02 19:08:48.062: I/HTTPRequestMaker(3415): sending request to http://10.0.1.101:7000/photo
//            07-02 19:08:48.062: I/AmbientMediaRuntime(3415): DomainRouter.addDevice for: [AirPlay] Apple TV with type MediaRenderer
//    07-02 19:08:48.062: I/AirPlayController(3415): Zeroconf calling notifyDeviceDiscovered for: [AirPlay] Apple TV
//    07-02 19:08:48.333: I/ardroneRuntime(3415): connected to: "Dynamix" waiting for drone to connect
//    07-02 19:08:49.334: I/ardroneRuntime(3415): connected to: "Dynamix" waiting for drone to connect
//    07-02 19:08:50.345: I/ardroneRuntime(3415): connected to: "Dynamix" waiting for drone to connect
//    07-02 19:08:50.375: E/ContextManager(3415): ContextPluginRuntime Exception: java.lang.NullPointerException
//    07-02 19:08:50.385: W/ContextManager(3415): disablePluginOnError for ContextPlugin: Name= ambient-media-plug-in | ID= org.ambientdynamix.contextplugins.ambientmedia | Version= 1.0.0| Repo source: /storage/emulated/0/dynamix/ with message ContextPlugin: Name= ambient-media-plug-in | ID= org.ambientdynamix.contextplugins.ambientmedia | Version= 1.0.0| Repo source: /storage/emulated/0/dynamix/ caused an error and was disabled
//    07-02 19:08:51.356: I/ardroneRuntime(3415): connected to: "Dynamix" waiting for drone to connect


    @Override
    public void onDeviceDetected(AmbientMediaDevice d) {
        try {
            Log.i(TAG, "onDeviceDetected for: " + d.getDisplayName() + " with type " + d.getType() + " and ID " + d.getId());
            // Update DomainRouter
            if (DomainRouter.addDevice(d)) {
                Log.i(TAG, "DomainRouter.addDevice for: " + d.getDisplayName() + " with type " + d.getType());
                // Send related context event
                MediaDeviceInfo info = new MediaDeviceInfo(d.getId(), d.getDisplayName(), d.getType(), true);
                d.setRuntime(this);

                sendBroadcastContextEvent(new SecuredContextInfo(info, PrivacyRiskLevel.LOW));


            } else
                Log.w(TAG, "DomainRouter didn't add " + d);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDeviceLost(String id) {
        // Access device
        if (DomainRouter.containsDevice(id)) {
            AmbientMediaDevice d = DomainRouter.getDevice(id);
            // Update DomainRouter
            DomainRouter.removeDevice(d);
            // Send related context event
            MediaDeviceInfo info = new MediaDeviceInfo(d.getId(), d.getDisplayName(), d.getType(), false);
            sendBroadcastContextEvent(new SecuredContextInfo(info, PrivacyRiskLevel.LOW));
        } else
            Log.w(TAG, "DomainRouter doesn't know about id " + id);
    }

    @Override
    public void start() {
        // Start each controller
        for (IMediaController c : mediaControllers) {
            c.start();
        }
        // Send events for all known devices
        for (AmbientMediaDevice d : DomainRouter.getAllDevices())
            onDeviceDetected(d);
        // Next, force a re-scan using the media controllers
        for (IMediaController c : mediaControllers)
            c.searchForDevices();
        // Startup the AirPlay service
        Log.d(TAG, "Started!");
    }

    @Override
    public void stop() {
        // Stop each controller
        for (IMediaController c : mediaControllers)
            c.stop();
        // Clear known devices
        DomainRouter.clearAllDevices();
        Log.d(TAG, "Stopped!");
    }

    @Override
    public void destroy() {
        // Call stop
        stop();
        // Close each controller
        for (IMediaController c : mediaControllers) {
            c.removeListener(this);
            c.close();
        }
        wifiManager = null;
        connectivityManager = null;
        Log.d(TAG, "Destroyed!");
    }

    @Override
    public void updateSettings(ContextPluginSettings settings) {
        // TODO Auto-generated method stub
    }

    @Override
    public void handleContextRequest(UUID requestId, String contextInfoType) {
        Log.i(TAG, "revceived request: " + contextInfoType);
        if (contextInfoType.equalsIgnoreCase("org.ambientdynamix.contextplugins.ambientmedia.discovery")) {
            // First send events for all known devices
            for (AmbientMediaDevice d : DomainRouter.getAllDevices()) {
                onDeviceDetected(d);
                sendContextEvent(requestId, new MediaDeviceInfo(d.getId(), d.getDisplayName(), d.getType(), true), PrivacyRiskLevel.LOW);
            }
            // Next, force a re-scan using the media controllers
            for (IMediaController c : mediaControllers)
                c.searchForDevices();
        } else {
            sendContextRequestError(requestId, "Context type not supported: " + contextInfoType,
                    ErrorCodes.CONTEXT_TYPE_NOT_SUPPORTED);
            return;
        }
        return;
    }

    @Override
    public boolean addContextlistener(ContextListenerInformation listenerInfo) {
        return controlConnectionManager.addContextListener(listenerInfo);
    }

    @Override
    public void onMessageReceived(Message message) {
        try {
            controlConnectionManager.handleConfigCommand(message);
        } catch (RemoteException e) {
            Log.e(TAG, e.toString());
            message.getResultHandler().onFailure(e.toString(), ErrorCodes.INTERNAL_PLUG_IN_ERROR);
        }
    }

    @Override
    public void handleConfiguredContextRequest(final UUID requestId, String contextInfoType, Bundle scanConfig) {
        Log.i(TAG, "revceived conf request: " + contextInfoType);
        if (!controlConnectionManager.handleConfiguredContextRequest(requestId, contextInfoType, scanConfig))
            if (contextInfoType.equalsIgnoreCase("org.ambientdynamix.contextplugins.ambientmedia.interaction")) {
                // Grab the scanConfig
                String interactionType = scanConfig.getString("interaction_type");
                if (interactionType != null) {
                    if (interactionType.equalsIgnoreCase("DeviceCommand")) {
                        Log.i(TAG, "Found interaction type: DeviceCommand");
                        // Grab DEVICE_ID and COMMAND
                        String deviceId = scanConfig.getString("device_id");
                        String command = scanConfig.getString("command");
                        Log.i(TAG, "command: " + command + " for device: " + deviceId);
                        // Check if the DomainRouter knows about the DEVICE_ID
                        if (DomainRouter.containsDevice(deviceId)) {
                            AmbientMediaDevice device = DomainRouter.getDevice(deviceId);
                            if (command != null) {
                                Log.i(TAG, "Processing command: " + command);
                                if (command.equalsIgnoreCase("play")) {
                                    // Grab the URI
                                    String uri = scanConfig.getString("uri");
                                    if (uri != null) {
                                        Log.i(TAG, "Got URI: " + uri);
                                        // Grab the TITLE
                                        String title = scanConfig.getString("title");
                                        if (title == null) {
                                            Log.w(TAG, "Missing TITLE");
                                            title = "";
                                        }
                                        // Call play on the device
                                        device.play(requestId, uri, title);
                                    } else {
                                        sendContextRequestError(requestId, "Missing URI", ErrorCodes.MISSING_PARAMETERS);
                                        return;
                                    }
                                } else if (command.equalsIgnoreCase("stop")) {
                                    // Call teardown on the device
                                    device.stop(requestId);
                                } else if (command.equalsIgnoreCase("pause")) {
                                    device.pause(requestId);
                                } else {
                                    sendContextRequestError(requestId, "Interaction not supported: " + command,
                                            ErrorCodes.NOT_SUPPORTED);
                                    return;
                                }
                            } else {
                                sendContextRequestError(requestId, "Missing COMMAND", ErrorCodes.MISSING_PARAMETERS);
                                return;
                            }
                        } else {
                            sendContextRequestError(requestId, "Could not find device for DEVICE_ID: " + deviceId,
                                    ErrorCodes.CONFIGURATION_ERROR);
                            return;
                        }
                    }
                } else {
                    sendContextRequestError(requestId, "Unsupported INTERACTION_TYPE: " + interactionType,
                            ErrorCodes.MISSING_PARAMETERS);
                    return;
                }
            } else {
                sendContextRequestError(requestId, "Context type not supported: " + contextInfoType,
                        ErrorCodes.CONTEXT_TYPE_NOT_SUPPORTED);
                return;
            }
        return;
    }

    public ControlConnectionManager getControlConnectionManager() {
        return controlConnectionManager;
    }
}