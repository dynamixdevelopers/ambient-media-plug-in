package org.ambientdynamix.contextplugins.ambientmedia.util;

import android.util.Log;
import org.ambientdynamix.contextplugins.ambientmedia.airplay.HTTPRequestMaker;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.*;

/**
 * Created by workshop on 12/5/13.
 */
public class YoutubeUrlParser {


    private final String TAG = this.getClass().getSimpleName();
    private URL youtubeUrl;

    public YoutubeUrlParser(URL youtubeUrl) {
        this.youtubeUrl = youtubeUrl;
    }

    public YoutubeUrlParser(String youtubeUrl) throws MalformedURLException {
        this.youtubeUrl = new URL(youtubeUrl);
    }

    public interface CallbackOnURLReady{
        public void callback(Map<String,String> atvSupportedUrls);
    }
    private CallbackOnURLReady callback;
    public void getQueryString(CallbackOnURLReady callback) throws UnsupportedEncodingException {
        this.callback = callback;
        Map<String, String> parameters = splitQuery(youtubeUrl.getQuery());
        if (parameters.containsKey("v")) {
            try {
                new HTTPRequestMaker("www.youtube.com").doHTTP("GET", "/get_video_info?video_id=" + parameters.get("v"), videoInfoParserCallback);
            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
            }
        } else
            throw new UnsupportedEncodingException("URL does not contain video parameter v=(videoId)");

    }

    private HTTPRequestMaker.Callback videoInfoParserCallback = new HTTPRequestMaker.Callback<String>() {
        @Override
        public void execute(String result) {
            Map<String,String> atvSupported = new HashMap<String, String>();
//            Log.i(TAG, "request result: " + result);
            try {
                Map<String, String> videoInfo = splitQuery(result);
                String url_encoded_fmt_stream_map = videoInfo.get("url_encoded_fmt_stream_map");
//                Log.i(TAG, "url_encoded_fmt_stream_map: " + url_encoded_fmt_stream_map);
                String[] urls = url_encoded_fmt_stream_map.split(",");
                for (int i = 0; i < urls.length; i++) {
//                    Log.i(TAG, "parsing: " + urls[i]);
                    Map<String, String> videoUrl = splitQuery(urls[i]);
                    String encodedVideoUrl = videoUrl.get("url");
                    String urlString = URLDecoder.decode(encodedVideoUrl, "UTF-8");
                    if(urlString.length()==0)
                        continue;
                    URL url;
                    try {
//                        Log.i(TAG, "looking at urlString: " + urlString);
                        url = new URL(urlString);
//                        url = new URL(url.toString().replace(",",""));
                    } catch (MalformedURLException e) {
                        continue;
                    }
//                    Log.i(TAG, "checking: " + url.toString());


                    if(videoUrl.containsKey("type")&& URLDecoder.decode(videoUrl.get("type"), "UTF-8").contains("video/mp4") &&
                            videoUrl.containsKey("itag")&& (videoUrl.get("itag").equals("22") || videoUrl.get("itag").equals("18"))){

                        String approvedURL = url.toString();
                        String quality = videoUrl.get("quality");


                        if(!videoUrl.containsKey("signature"))
                            approvedURL = approvedURL + "&signature=" + videoUrl.get("sig");
                        approvedURL.replaceAll("&itag=" + videoUrl.get("itag"),"");
//                        Log.i(TAG, "atv Url with " + quality + " quality: " + approvedURL);
                        atvSupported.put(videoUrl.get("itag"), approvedURL);
                    }
//                    else
//                        Log.i(TAG, "not atv compatible: " + url.toString());
                }
                callback.callback(atvSupported);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }

        ;
    };

    public static Map<String, String> splitQuery(String query) throws UnsupportedEncodingException {
        Map<String, String> query_pairs = new LinkedHashMap<String, String>();
        String[] pairs = query.split("&");
        for (String pair : pairs) {
            int idx = pair.indexOf("=");
            query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));

//            Log.i("splitQuery","decoded key " +   pair.substring(0, idx) + " to " + URLDecoder.decode(pair.substring(0, idx), "UTF-8") + " with value: " +
//                    pair.substring(idx + 1).substring(0, Math.min(pair.substring(idx + 1).length(), 500)) + "                TOOOOOOOO          "+
//                    URLDecoder.decode(pair.substring(idx + 1), "UTF-8").substring(0,Math.min(URLDecoder.decode(pair.substring(idx + 1), "UTF-8").length(), 500)));
        }
        return query_pairs;
    }
}
